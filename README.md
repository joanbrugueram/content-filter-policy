# ContentFilterPolicy

## What is it?

ContentFilterPolicy is similar to a content blocker (aka. adblocker), but with a focus on providing additional features other than plainly blocking content. Calling it a customizable content filter would be a  much better fit.

**WORK IN PROGRESS: Not ready for any real use.**

Planned features / Ideas:

* Support for AdBlock Plus filter lists.

* Customizable 'content filtering' (ability to apply user-defined options to filtered elements), e.g.:

  * Hiding filtered content

  * Highlighting filtered content

  * 'Click-to-play' for filtered content

* Mandatory 'sandboxing' (blocking features) to filtered scripts, such as:

  * AJAX requests

  * Cookies

  * Mouse tracking

  * Etc.

* Being transparent to filtered websites (unless opting out), in order to avoid breakage.

## How does it work (User level)

You will need to start a process that acts as a proxy server and configure your browser to use this proxy server. This proxy will filter the content you visualize on websites according to the settings defined on a configuration file you can edit.

## How does it work (Technical level)

See TECHNICAL-DETAILS.md

## Quickstart instructions

### Test cases

This will allow you to set up a server in your local network that contains various content filter tests, in order to test the proxy.

1. Ensure you have node.js and npm installed and open a console window.

2. Go to the test cases directory: `cd /path/to/my/repo/test_cases`

3. Install dependencies and run: `npm install && npm run start`

4. Browse using your web browser to http://YOUR.IP.ADDRESS.HERE:8080 to see the test case list.

### ContentFilterPolicy proxy

This will allow you to set up a proxy server in your local network that allows you apply policies to filtered content.

1. Ensure you have node.js and npm installed and open a console window.

2. Go to the proxy server directory: `cd /path/to/my/repo/content-filter-policy`

3. Install dependencies and run: `npm install && npm run start`

4. Set up your web browser to use the proxy at YOUR.IP.ADDRESS.HERE:8081.

5. Browse to the location you wish to proxy (e.g. the test cases above) to observe the results.

**TIP**: If you don't observe the expected results, try clearing your browser's cache.

### Further setup: SSL Proxying (HTTPS websites)

You will have to add the CA certificate that node-mitm-proxy generated to your browser as a trusted CA in order to proxy secure websites.

You can find the CA certificate you have to install in the `.http-mitm-proxy/certs/ca.pem` directory under the proxy directory.

Exact instructions depend on your browser. Searching for 'install pem certificate [your-browser-name]' should be helpful.

### Further setup: Configuration file

You can create a `proxyConfig.json` file based on the default `proxyConfig.json.defaults` file, in order to customize how the proxy works.

You can find additional information about the configurable behaviours in the `proxyConfig.json.defaults` file as comments.

## Browser support

Currently only tested the latest versions of Chrome and Firefox. May work or be easily portable the latest versions of Edge and Safari. Legacy browser support is not to be expected.

## License

GPL v3 (See COPYING).
