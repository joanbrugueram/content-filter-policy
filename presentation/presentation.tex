\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\definecolor{originalwebsitenodes}{HTML}{369100}
\definecolor{adblockernodes}{HTML}{FF0000}

\usetheme{Berlin}

\title[Online ad-blocking detection: The state of the art for counter-detection and a PoC for new approaches]{Online advertisement blocker detection: \\ A look at the state of the art for counter-detection and a proof-of-concept for new approaches}
\author[Joan Bruguera Micó]{Joan Bruguera Micó \\ {\scriptsize Directed by Llu\'is Garrido}}
\institute
{
  Facultat de Matemàtiques i Informàtica\\ Universitat de Barcelona
}
\date{Undergraduate thesis \\ Mathematics and Computer Science Degree}
\subject{Undergraduate thesis \\ Mathematics and Computer Science Degree}

\begin{document}
\frame{\titlepage}

\begin{frame}
\frametitle{Table of Contents}
\tableofcontents
\end{frame}

\section{Context}

\begin{frame}
\frametitle{Context: Online advertisement blocking}

\begin{itemize}
\item Online advertising has been used by a multitude of websites as a source of revenue.
\item However, in the recent years, there has been a huge rise in adoption of advertisement blocking software (\emph{ad-blockers}).
\end{itemize}

Reasons include:
\begin{itemize}
\item Avoiding distractions while surfing the web
\item Reduced data usage and increased performance
\item Association of advertisement with privacy and security threats (tracking/malvertisement)
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Context: Online advertisement blocking}

\begin{figure}
\caption{\textbf{Adoption of ad-blocking} among internet users. {\tiny Source: PageFair}}
\includegraphics[width=\linewidth]{adblockingadoption.png}
\end{figure}

\end{frame}

\begin{frame}
\frametitle{Context: Online advertisement blocking detection}

\begin{columns}[onlytextwidth]
\begin{column}{0.6\textwidth}
\begin{itemize}
\item As a reaction, some websites have started adopting \textbf{ad-blocker detection techniques}, which aim to detect whether the user that accesses the website has an ad-blocker installed, and change its behavior.
\item As a further reaction, there have been some initiatives by ad-blocker creators and user communities to create \textbf{anti-ad-blocker detection software}.
\end{itemize}
\end{column}

\begin{column}{0.4\textwidth}
\begin{figure}
\caption{Sample \textbf{ad-blocking detector}}
\includegraphics[width=\linewidth]{adblockdetectorblocking.png}
\end{figure}
\end{column}
\end{columns}

\end{frame}

\begin{frame}
\frametitle{Context: Online advertisement blocking}

\begin{figure}
\caption{\textbf{States of the ad-blocker wars}. {\tiny Adapted from: The Future of Adblocking}}
\includegraphics[width=0.4\linewidth]{adblockingstates.png}
\end{figure}

\end{frame}

\section{State of the art}

\begin{frame}
\frametitle{State of the art: Anti-ad-blocking detection software}

Techniques and software such as anti-ad-blocker detection filter lists, \emph{Anti-Adblock Killer}, \emph{AdBlock Protector}, etc., exist. \\~\\

They work by blocking, preventing execution, or 'tricking' of ad-blocker detection scripts. \\~\\

As a consequence, they have limitations:

\begin{itemize}
\item They require creating a specific "workaround" for every different ad-blocker detector.
\item Creating such workarounds is a manual, time-consuming task which requires technical knowledge.
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{State of the art: Fundamental approach to the problem}

\begin{itemize}
\item The fundamental problem is that \textbf{the changes ad-blockers do to the website layout are visible to the website scripts}.
\item A generic approach needs to \textbf{isolate the changes ad-blockers do} to the website model from the website scripts
\end{itemize}

\begin{figure}
\includegraphics[width=\linewidth]{modelconventional.png} \\
\textcolor{originalwebsitenodes}{\textbf{Green:}} Original website nodes, \textcolor{adblockernodes}{\textbf{Red:}} Ad-blocker modified nodes
\end{figure}

\end{frame}

\begin{frame}
\frametitle{State of the art: Research (\emph{The Future of Ad Blocking})}

\begin{itemize}
\item Recently (circa April 2017), a research paper was published that aims to do this.
\item The researchers let the advertising scripts run normally, however, \textbf{they mask the advertisements} by placing overlays over them.
\item The researchers presented techniques that allows the ad-blocker to \textbf{make a part of the website model inaccessible to the website scripts}, in which the overlays over the advertisements can be placed.
\end{itemize}



\end{frame}

\begin{frame}
\frametitle{State of the art: Research (\emph{The Future of Ad Blocking})}

\begin{figure}
\includegraphics[width=\linewidth]{modelfutureadblocking.png}
\end{figure}

\end{frame}

\section{Motivation and objectives}

\begin{frame}
\frametitle{Motivation and objectives: What can be improved?}

{\Large The techniques presented in that paper are \textbf{a huge progress} in this direction, but there are \textbf{further improvements} that can be done.}

\end{frame}

\begin{frame}
\frametitle{Motivation and objectives: Layout}

{\Large \textbf{Layout:} How can we completely hide the advertisements visually, instead of covering them?}

\begin{figure}
\includegraphics[width=0.6\linewidth]{adblocklayout.png}
\end{figure}

\end{frame}

\begin{frame}
\frametitle{Motivation and objectives: Privacy}

{\Large \textbf{Privacy:} Since we're allowing the advertising scripts to run, how can their privacy impact be limited?}

\begin{figure}
\includegraphics[width=\linewidth]{privacyapisconventional.png}
\end{figure}

\end{frame}

\begin{frame}
\frametitle{Motivation and objectives: Data usage and performance}

{\Large \textbf{Data usage and performance:} Since we're allowing advertising scripts to run, what can we do to improve data usage and performance?}

\begin{figure}
\includegraphics[width=\linewidth]{performancemotivation.png}
\end{figure}

\end{frame}

\section{Proposal}

\begin{frame}
\frametitle{Proposal: What we aim to do.}

{\Large We aim to both \textbf{present techniques} to advance on those aspects, and implement a \textbf{working prototype} using those techniques.}

\end{frame}

\begin{frame}
\frametitle{Proposal: Layout}

{\Large \textbf{Layout:} Hide the advertisements like regular ad-blockers do (instead of masking the advertisements), but virtually redisplay them when a website script executes}

\begin{figure}
\includegraphics[width=\linewidth]{modelprototype.png}
\end{figure}

\end{frame}

\begin{frame}
\frametitle{Proposal: Privacy}

{\Large \textbf{Privacy:} Restrict access to privacy-sensitive APIs, by detecting when one of those APIs is accessed by an advertiser script and returning fake data in this case}

\begin{figure}
\includegraphics[width=\linewidth]{privacyapisprototype.png}
\end{figure}

\end{frame}

\begin{frame}
\frametitle{Proposal: Data usage and performance}

{\Large \textbf{Data usage and performance:} Ability to mark a website (or part) as using anti-ad-blocker detector techniques in order to reduce the data usage and performance impact of the previous techniques}

\begin{figure}
\includegraphics[width=0.9\linewidth]{performanceproposal.png}
\end{figure}

\end{frame}

\section{Implementation}

\begin{frame}
\frametitle{Implementation: Core}

\begin{itemize}

\item Our prototype basically works like any other regular filter list-based ad-blocker
\begin{itemize}
\item Technically similar to uBlock Origin, AdBlock Plus, etc.
\end{itemize}
\item It is implemented as a man-in-the-middle proxy server that injects the ad-blocker code to the requested websites
\item Implements the techniques we explained previously

\end{itemize}

\begin{columns}[onlytextwidth]
\begin{column}{0.25\textwidth}
\begin{figure}
\includegraphics[width=2cm]{adblockpluslogo.png}
\end{figure}
\end{column}
\begin{column}{0.25\textwidth}
\begin{figure}
\includegraphics[width=2cm]{ublockoriginlogo.png}
\end{figure}
\end{column}
\begin{column}{0.25\textwidth}
\begin{center}
{\fontsize{5cm}{5.5cm}\selectfont +}
\end{center}
\end{column}
\begin{column}{0.25\textwidth}
\vspace*{-0.2in}
\begin{figure}
\includegraphics[width=3cm]{modelprototype.png} \\
\includegraphics[width=3cm]{privacyapisprototype.png}
\end{figure}
\end{column}
\end{columns}

\end{frame}

\begin{frame}
\frametitle{Implementation: Website script context detection}

\begin{itemize}
\item We (ab)use browser APIs and the flexibility of JavaScript to know when a script in the website starts executing.
\item When a website script starts executing, we redisplay the advertisements.
\item We then allow the website script to execute normally.
\item Finally, when a website script ends executing, we hide the advertisements again.
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Implementation: Website script context detection (details)}

\begin{itemize}
\item How to know when a website script starts executing?
\begin{itemize}
\item We can "monkey-patch" the browser DOM APIs in order to receive a callback when a website first accesses them.
\item Redefine \texttt{HTMLElement.prototype} using \texttt{Object.defineProperty(...)}
\end{itemize}
\item How to know when a website script ends executing?
\begin{itemize}
\item Some actions are delayed until the end of the scripts (e.g. resolved \texttt{Promise}s).
\end{itemize}
\item Redisplaying the advertisements momentously doesn't cause any visual effect, as long as we time it correctly to avoid a browser render.
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Implementation: Advertiser script context detection}

\textbf{Idea:}

\begin{itemize}
\item We know which scripts belong to advertisements since they are defined in the ad-blocker filter lists.
\item We "monkey-patch" privacy-sensitive APIs in order to receive a callback when one of them is called.
\begin{itemize}
\item Using \texttt{Object.defineProperty(...)}.
\end{itemize}
\item When we receive a call to a privacy-sensitive APIs, we check if it is one of the advertiser script by walking the call stack.
\begin{itemize}
\item If the call comes from an advertiser script, we return fake data from it.
\item Otherwise we call the real API.
\end{itemize}
\end{itemize}

\end{frame}

\section{Results and conclusions}

\begin{frame}
\frametitle{Results}

\begin{itemize}
\item We designed an extensive test set (42 cases) against which to test our ad-blocker prototype.
\item In addition, we tested it against ready-made ad-blocker detector solutions and real websites.
\item We achieved good results ($\sim$90\%) with our prototype on real websites.
\item We also traced all failures to limitations of our prototype implementation and not to fundamental limitations of our techniques.
\item Performance impact was noticeable. Can be improved with a more thoughtful implementation.
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Conclusions}

\begin{itemize}
\item We proposed and implemented techniques that aim to improve the state of the art of anti-ad-blocker detector techniques.
\item Some work still needs to be done to refine the techniques (implementation and performance-wise) and to integrate them into mainstream ad-blockers.
\item The "ad-blocking wars" are not over. Publishers and advertisers can easily move to native advertising, DRM, etc.
\end{itemize}

\end{frame}

\section{Demonstration}

\begin{frame}
\frametitle{Demonstration}

{\fontsize{5cm}{5.5cm}\selectfont Demo time}

\end{frame}

\section{}


\begin{frame}
\frametitle{Thank you!}

{\fontsize{5cm}{5.5cm}\selectfont Thank you for your attention!}

\end{frame}



\end{document}
