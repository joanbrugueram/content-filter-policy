window.lastTrackMousePos = null;

window.addEventListener("load", function() {
    document.addEventListener("mousemove", function(e) {
        window.lastTrackMousePos = [e.clientX, e.clientY];
    });
});
