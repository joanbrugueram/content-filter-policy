const http = require('http');
const finalhandler = require('finalhandler');
const serveStatic = require('serve-static');
const fs = require('fs');
const url = require('url');
const ip = require('ip');

const serve = serveStatic('.', {'index': ['index.html', 'index.htm']});

const port = 8080;
const caseListJsonPath = '/case_list.json';

const server = http.createServer(function(request, response) {
    request.addListener('end', function() {
        if (url.parse(request.url).pathname === caseListJsonPath) {
            const dirList = fs.readdirSync('.').filter(
                file => fs.statSync(file).isDirectory() &&
                fs.existsSync(file + '/index.html'));

            response.writeHead(200, {'Content-Type': 'application/json'});
            response.end(JSON.stringify(dirList));
        } else {
            serve(request, response, finalhandler(request, response));
        }
    }).resume();
}).listen(port);

console.log('<----------------------------------->');
console.log('< CONTENT-REQUEST-POLICY TEST CASES >');
console.log('<----------------------------------->');
console.log('Server running. Connect to http://' + ip.address() + ':' + port + ' to see the test cases.');
