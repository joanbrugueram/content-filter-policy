"use strict";

function TestClass() { }
TestClass.prototype.method = function(elm) {
    var window = 123;

    if (window !== 123) {
        elm.classList.add("test-result-ko");
    }
}

window.addEventListener('load', function (){
    var x = new TestClass();
    x.method(document.getElementsByClassName("test-result")[0]);
});
