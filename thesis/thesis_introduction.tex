\section{Introduction}

A big proportion of Internet websites nowadays use advertisements as a source of revenue. However, many users consider those advertisements unpleasant, since they are commonly perceived as being distracting or irrelevant, and they have been commonly associated with increased power use and other privacy and security risks.

As a result, \textbf{a multitude of advertisement blocker software} (also known as ad-blockers) has been developed, \textbf{which aims to block advertisements} (and typically also block other risks, such as malware risks or trackers) to some degree. Such ad-blockers have been adopted by a notable portion of Internet users.

As a reaction to the increasing prevalence of ad-blockers, \textbf{some websites have started using ad-blocker detectors}, which are pieces of code that are able to detect whether or not the user has an ad-blocker or not, and change the behavior of the website accordingly to the result. As a further reaction to ad-blocking detection, \textbf{various anti-ad-blocker detector techniques and software have been created}, which aim to make a website unable to detect whether an user or not has an ad-blocker installed. \\

Ad-blocker detectors can exist because \textbf{contemporary ad-blockers don't isolate themselves from the website, but rather act on the same internal representation of the website} (the document object model, or DOM) that the websites can access, and thus make their actions easily detectable by scripts acting on the website. On the other hand, \textbf{contemporary anti-ad-blocker detectors don't attempt to fundamentally solve this problem}, but rather work by crafting manual workarounds that impede the functioning of single ad-blocker detector implementations.

Recently, there have been early research efforts into fundamentally solving this problem. As recently as April 2017, \textbf{a paper explored using APIs that modern web browsers support in order to isolate part of the DOM tree from the website scripts, in order to make their ad-blocker implementation undetectable while masking the ads}. While the techniques given by the researchers effectively achieve this purpose, \textbf{there is some room for improvement, since the implementation given by the researchers doesn't have some properties desired by users}, such as altering the website layout when blocking advertisements (instead of masking them), or dealing with privacy and data usage. \\

In this thesis, \textbf{we aim to build on those techniques and propose practical ways to improve on those qualities}.

We explore ways to \textbf{allow altering the website layout when blocking advertisements}, by dynamically altering the state the representation of the website when a website script starts or ends execution in order to be able to isolate the changes applies by an ad-blocker. In addition, we also \textbf{explore ways to minimize privacy risks} by providing ways to detect and restrict access to privacy-sensitive APIs by advertising/tracking scripts.

In addition, \textbf{we provide a prototype implementation of the techniques} we describe, which works under modern web browsers, and we evaluate the results and limitations of those techniques when applied to existing ad-blocker detectors. Finally, we explain the conclusions of our work and consider future directions in this area.

\subsection{Structure of the thesis}

As for the structure of our thesis, we start by introducing and explaining the current state of Internet advertising: Both the origin and motivation of Internet advertising, the origin and motivation and Internet advertisement blocking, and the reactions to Internet advertisement blocking by website owners and advertisers (see~\ref{sec:thesis_internetadvertising}). \\

Next, we explain the technical principles of this kind of software (see~\ref{sec:thesis_principlesadblockingsoftware}) that allow it to filter online advertisements, and how ad-blocker detectors work. We also take a look at the existing established advertisement blocking software and its features. To continue, we evaluate the current state of the art of software techniques to avoid detection by ad-blocker detectors and we explain the motivation for proposing a new set of techniques for avoiding detection by ad-blocker detectors, and an overview of their operation. \\

We then explain in greater detail how we implemented those techniques in an ad-blocker prototype that avoids detection by ad-blockers and the difficulties we have had to deal with during their implementation (see~\ref{sec:thesis_technicaldetails}). \\

Finally, we evaluate the results of our ad-blocker prototype against various tests (see~\ref{sec:thesis_results}), and we explain the limitations of our proposed techniques and our implementation of our ad-blocker prototype (see~\ref{sec:thesis_limitations}). We end by summarizing our results and reaching a conclusion (see~\ref{sec:thesis_conclusions}).
