\section{Technical principles of ad-blocking software}
\label{sec:thesis_principlesadblockingsoftware}

\subsection{DOM Model for Web Sites}

Websites communicate with the browser to specify the model for the objects in the website using a structure called the DOM (short for 'Document Object Model'). \\

In simple terms, the DOM consists of a tree structure that contains the various objects necessary to display the website, such as the contents of the website (text, images, etc.), special tags that specify how the website should be presented (titles, paragraphs, tables, etc.), and other resources that alter the presentation or behavior of the web site (style sheets, scripts, etc.).

Generally, in web sites, the initial state of the DOM is defined by an HTML document, but the DOM can be modified by other elements (scripts, style sheets, etc.). In addition, elements in the DOM can specify that some resource should be downloaded from an external web address.

\begin{figure}[H]
  \centering
  \caption{\textbf{Example DOM tree}}
  \begin{forest}
  for tree={
    domnode
  },
  forked edges,
  [<html>
    [<head>
      [<title>
        ["Web Site", domtextnode]
      ]
      [<script src{=}'main.js'>
        [(Contents of \\ external resource), domtextnode]
      ]
      [<script src{=}'adtrack.js'>
        [(Contents of \\ external resource), domtextnode]
      ]
    ]
    [<body>
      [<h1>
        ["My website", domtextnode]
      ]
      [<p>
        ["You will find \\ many interesting \\ things here", domtextnode]
      ]
      [<div class 'adsbox'>
        ["Advertisements \\ go here", domtextnode]
      ]
    ]
  ]
  \end{forest}
\end{figure}

\subsection{Ad-blocking Filter Lists}

The majority and the most popular of ad-blocking software in use today works by using what is known as \emph{filter lists}. Filter lists are lists are lists (blacklists) of blocking rules that specify which parts should be blocked from the websites the user visits. Those lists are typically maintained manually by various users and entities.

There are various formats for filter lists with different features. The most popular one, which we will take as a reference, is the AdBlock Plus filter list format~\cite{adblockplusfilters}, which is used by the most popular blockers such as \emph{AdBlock Plus}~\cite{adblockplus} and \emph{uBlock Origin}~\cite{ublockorigin} and the most used filter lists such as \emph{EasyList}~\cite{easylist}.

\subsubsection{Address blocking rules}

Address blocking rules work at the network level, by blocking requests to certain URLs or entire domains.

Due to the usage of advertisement networks by a majority of websites using advertisements nowadays, address blocking rules are often very effective against advertisements. This is because, to use third-party ads, a website will often include a reference to an script from the third-party domain that will then embed the advertisements in the website. By blocking the requests to the third-party domains, advertisements from those third-parties can be effectively blocked.

\begin{figure}[H]
  \centering
  \caption{\textbf{Example DOM tree, in which a script resource has been blocked by address blocking rules}}
  \begin{forest}
  for tree={
    domnode
  },
  forked edges,
  [<html>
    [<head>
      [<title>
        ["Web Site", domtextnode]
      ]
      [<script src{=}'main.js'>
        [(Contents of \\ external resource), domtextnode]
      ]
      [<script src{=}'adtrack.js'>, domnodedisabled
        [(Blocked), domnodedisabled, node options={dashed}, edge=dashed]
      ]
    ]
    [<body>
      [<h1>
        ["My website", domtextnode]
      ]
      [<p>
        ["You will find \\ many interesting \\ things here", domtextnode]
      ]
      [<div class 'adsbox'>
        ["Advertisements \\ go here", domtextnode]
      ]
    ]
  ]
  \end{forest}
\end{figure}

\subsubsection{Element hiding rules}

Element hiding rules work at the website (DOM) level, by hiding elements in the DOM that are identified as advertisements.

Elements within a website are identified by using CSS Selectors~\cite{mozilladevelopernetwork}. CSS Selectors allow selecting a website's elements according to various criteria, such as its position within the DOM tree, or one of its attributes (tag name, identifier, class, etc.).

Element hiding rules are typically used for blocking elements that are difficult to block with address blocking rules, such as first-party advertisements, or to remove the elements where third-party ads are embedded (such as \emph{pop-unders}~\cite{webopedia}, etc.)

\begin{figure}[H]
  \centering
  \caption{\textbf{Example DOM tree, in which an element has been hidden by element hiding rules}}
  \begin{forest}
  for tree={
    domnode
  },
  forked edges,
  [<html>
    [<head>
      [<title>
        ["Web Site", domtextnode]
      ]
      [<script src{=}'main.js'>
        [(Contents of \\ external resource), domtextnode]
      ]
      [<script src{=}'adtrack.js'>
        [(Contents of \\ external resource), domtextnode]
      ]
    ]
    [<body>
      [<h1>
        ["My website", domtextnode]
      ]
      [<p>
        ["You will find \\ many interesting \\ things here", domtextnode]
      ]
      [<div class 'adsbox'>, domnodedisabled
        [(Hidden), domnodedisabled, node options={dashed}, edge=dashed]
      ]
    ]
  ]
  \end{forest}
\end{figure}

\subsection{How Ad-blocking detection scripts work}

Ad-blocking detection scripts are JavaScript scripts that are inserted inside a website that check whether or not an ad-blocker is running.

While most popular ad-blockers don't expose any explicit programming interface that allows checking if it is running, web browsers expose many mechanisms that scripts can use to detect if one is running, by checking if known filter rules defined in filter lists are being applied.

A more in-depth analysis of ad-blocking detectors can be seen in the paper \emph{A First Look at Ad-block Detection –
A New Arms Race on the Web}~\cite{afirstlookatadblockdetection}

\subsubsection{Address blocking rules}

One way an script can detect if an ad-blocker is running is by checking whether some requests are being blocked by \textbf{address blocking rules}. As a non-exhaustive list, an script can check any of the following to check whether a resource is being blocked by address blocking rules:

\begin{itemize}

\item Whether a request to the resource completes successfully.

\item Whether the content returned by the request is the expected content.

\item Whether a global variable defined in an included filtered script has been defined.

\item Whether a type defined in an included filtered script exists.

\end{itemize}

It's worth noting that detecting ad-blockers by address blocking rules can be unreliable, due to various other factors that can interfere with web requests. For example, factors such as server downtime, unstable wireless networks, network firewalls, etc., could cause a false-positive, i.e. a user without an ad-blocker to be affected.

\textbf{Example:} For example, suppose that a filter list blocks all scripts named \texttt{adtrack.js}. Then, one can create a "bait script" named \texttt{adtrack.js} that defines a global variable called \texttt{VAR} and include that "bait script" in the website.

Then, inside another script (perhaps mixed with the rest of the scripts necessary to display the page), one can check if the \texttt{VAR} global variable is defined. If it isn't, then one can consider that the request to the "bait script" has been blocked by an ad-blocker and change the behavior of the page accordingly. \\

\begin{figure}[H]
  \centering
  \caption{\textbf{Ad-blocker detection by address blocking, without an ad-blocker}}
  \begin{changemargin}{-0.5cm}{-0.5cm}
  \begin{tikzpicture}[thick,scale=0.85, every node/.style={transform shape}]
  \node[inner sep=0pt, align=center] (html) at (0,-3)
    {\includegraphics[width=.1\textwidth]{iconhtml.png}\\Website HTML};
  \node[inner sep=0pt, align=center] (resource1) at (2.75,0)
    {\includegraphics[width=.1\textwidth]{iconcss.png}\\Resource};
  \node[inner sep=0pt, align=center] (resource2) at (2.75,-3)
    {\includegraphics[width=.1\textwidth]{iconjs.png}\\Resource};
  \node[inner sep=0pt, align=center] (resourcebaitscript) at (2.75,-6)
    {\includegraphics[width=.1\textwidth]{iconjs.png}\\Bait Script\\Defines \texttt{VAR}};
  \node[inner sep=0pt, align=center] (browserwithads) at (6,-3)
    {\includegraphics[width=.15\textwidth]{browserwithads.png}\\Website with ads};
  \node[inner sep=0pt, align=center] (adblockerdetector) at (10,-3)
    {\includegraphics[width=.1\textwidth]{iconjs.png}\\Ad-Blocker Detector};
  \node[inner sep=0pt, align=center] (browser) at (10,-6)
    {\includegraphics[width=.1\textwidth]{iconbrowser.png}\\Web Browser};
  \node[inner sep=0pt, align=center] (adblockerdetected) at (15,-3)
    {\includegraphics[width=.1\textwidth]{iconok.png}\\Ad-Blocker not detected};
  \draw[->,thick] (html.10) -- (resource1.west);
  \draw[->,thick] (html.0) -- (resource2.west);
  \draw[->,thick] (html.-10) -- (resourcebaitscript.west);
  \draw[->,thick] (resource1.east) -- (browserwithads.170);
  \draw[->,thick] (resource2.east) -- (browserwithads.180);
  \draw[->,thick] (resourcebaitscript.east) -- (browserwithads.190);
  \draw[->,thick] (browserwithads.east) -- (adblockerdetector.west);
  \draw[->,thick] (adblockerdetector.230) -- (browser.130)
      node[midway,fill=white] {\texttt{VAR} defined?};
  \draw[->,thick] (browser.50) -- (adblockerdetector.310)
      node[midway,fill=white] {Yes};
  \draw[->,thick] (adblockerdetector.east) -- (adblockerdetected.west);
  \end{tikzpicture}
  \end{changemargin}
\end{figure}

\begin{figure}[H]
  \centering
  \caption{\textbf{Ad-blocker detection by address blocking, with an ad-blocker}}
  \begin{changemargin}{-0.5cm}{-0.5cm}
  \begin{tikzpicture}[thick,scale=0.85, every node/.style={transform shape}]
  \node[inner sep=0pt, align=center] (html) at (0,-3)
    {\includegraphics[width=.1\textwidth]{iconhtml.png}\\Website HTML};
  \node[inner sep=0pt, align=center] (resource1) at (2.75,0)
    {\includegraphics[width=.1\textwidth]{iconcss.png}\\Resource};
  \node[inner sep=0pt, align=center] (resource2) at (2.75,-3)
    {\includegraphics[width=.1\textwidth]{iconjs.png}\\Resource};
  \node[inner sep=0pt, align=center] (resourcebaitscript) at (2.75,-6)
    {\includegraphics[width=.1\textwidth]{iconadblocker.png}\\Bait Script\\(Blocked)};
  \node[inner sep=0pt, align=center] (browserwithoutads) at (6,-3)
    {\includegraphics[width=.15\textwidth]{browserwithoutads.png}\\Website without ads};
  \node[inner sep=0pt, align=center] (adblockerdetector) at (10,-3)
    {\includegraphics[width=.1\textwidth]{iconjs.png}\\Ad-Blocker Detector};
  \node[inner sep=0pt, align=center] (browser) at (10,-6)
    {\includegraphics[width=.1\textwidth]{iconbrowser.png}\\Web Browser};
  \node[inner sep=0pt, align=center] (adblockernotdetected) at (15,-3)
    {\includegraphics[width=.1\textwidth]{iconko.png}\\Ad-Blocker detected};
  \draw[->,thick] (html.10) -- (resource1.west);
  \draw[->,thick] (html.0) -- (resource2.west);
  \draw[->,thick] (html.-10) -- (resourcebaitscript.west);
  \draw[->,thick] (resource1.east) -- (browserwithoutads.170);
  \draw[->,thick] (resource2.east) -- (browserwithoutads.180);
  \draw[->,thick] (browserwithoutads.east) -- (adblockerdetector.west);
  \draw[->,thick] (adblockerdetector.230) -- (browser.130)
      node[midway,fill=white] {\texttt{VAR} defined?};
  \draw[->,thick] (browser.50) -- (adblockerdetector.310)
      node[midway,fill=white] {No};
  \draw[->,thick] (adblockerdetector.east) -- (adblockernotdetected.west);
  \end{tikzpicture}
  \end{changemargin}
\end{figure}

\begin{figure}[H]
  \caption{\textbf{Sample JavaScript code for an ad-blocker detector based on address blocking rules. The address rule 'adtrack.js' is included in the popular \emph{EasyList} filter list, and therefore, that script is not executed under the presence of an ad-blocker.}}

  \begin{changemargin}{1cm}{1cm}
  {\large index.html:}
  \begin{changemargin}{1cm}{1cm}
  \begin{minted}[linenos,
                 frame=lines]{html}
<script src='adtrack.js'></script>
<script>
if (window.requestLoaded !== true)
    alert('Ad-Blocker Detected');
</script>
  \end{minted}
  \end{changemargin}

  {\large adtrack.js:}
  \begin{changemargin}{1cm}{1cm}
  \begin{minted}[linenos,
                 frame=lines]{javascript}
var requestLoaded = true;
  \end{minted}
  \end{changemargin}
  \end{changemargin}
\end{figure}

\subsubsection{Element hiding rules}

Another way a script can detect if an ad-blocker is running is by checking whether some DOM elements are being hidden by \textbf{element hiding rules}. As a non-exhaustive list, an script can check any of the following to check whether an element is being blocked by address blocking rules:

\begin{itemize}

\item Whether an element is considered visible by the browser.

\item Whether an element's height is the expected value (a hidden element is considered to have a height of 0 by the browser)

\item Whether the page's elements are positioned as expected (hiding elements can change the page's layout).

\end{itemize}

Unlike the case with address blocking rules, one can be certain that an ad-blocker or a similar technology is present if detected by such a method, since web browsers have a clearly defined behavior for DOM elements, and there are no other external elements (other than the ad-blocker) that should be able to interfere with it.

\textbf{Example:} For example, suppose that a filter list blocks all DOM elements that have the CSS class \texttt{adsbox}. Then, inside an script (perhaps mixed with the rest of the scripts necessary to display the page), one can create a "bait element" assigning the CSS class \texttt{adsbox} to it.

After a short period of time (in order to allow the ad-blocker to run), one script can ask the web browser whether the "bait element" is visible or not. If it isn't, , then one can consider that the "bait element" has been hidden by an ad-blocker and change the behavior of the page accordingly.

\begin{figure}[H]
  \centering
  \caption{\textbf{Ad-blocker detection by element hiding, without an ad-blocker}}
  \begin{tikzpicture}[thick,scale=0.85, every node/.style={transform shape}]
  \node[inner sep=0pt, align=center] (browserwithads) at (0,0)
    {\includegraphics[width=.15\textwidth]{browserwithads.png}\\Website};
  \node[inner sep=0pt, align=center] (adblockerdetector) at (5,0)
    {\includegraphics[width=.1\textwidth]{iconjs.png}\\Ad-Blocker Detector};
  \node[inner sep=0pt, align=center] (browser) at (5,-3)
    {\includegraphics[width=.1\textwidth]{iconbrowser.png}\\Web Browser};
  \node[inner sep=0pt, align=center] (adblockernotdetected) at (10,0)
    {\includegraphics[width=.1\textwidth]{iconok.png}\\Ad-Blocker not detected};
  \draw[->,thick] (browserwithads.east) -- (adblockerdetector.west);
  \draw[->,thick] (adblockerdetector.230) -- (browser.130) node[midway,fill=white] {Ad visible?};
  \draw[->,thick] (browser.50) -- (adblockerdetector.310) node[midway,fill=white] {Yes};
  \draw[->,thick] (adblockerdetector.east) -- (adblockernotdetected.west);
  \end{tikzpicture}
\end{figure}

\begin{figure}[H]
  \centering
  \caption{\textbf{Ad-blocker detection by element hiding, with an ad-blocker}}
  \begin{changemargin}{-0.5cm}{-0.5cm}
  \begin{tikzpicture}[thick,scale=0.85, every node/.style={transform shape}]
  \node[inner sep=0pt, align=center] (browserwithads) at (0,0)
    {\includegraphics[width=.15\textwidth]{browserwithads.png}\\Website};
  \node[inner sep=0pt, align=center] (adblocker) at (2.75,0)
    {\includegraphics[width=.1\textwidth]{iconadblocker.png}\\Ad-Blocker};
  \node[inner sep=0pt, align=center] (browserwithoutads) at (6,0)
    {\includegraphics[width=.15\textwidth]{browserwithoutads.png}\\Website without ads};
  \node[inner sep=0pt, align=center] (adblockerdetector) at (10,0)
    {\includegraphics[width=.1\textwidth]{iconjs.png}\\Ad-Blocker Detector};
  \node[inner sep=0pt, align=center] (browser) at (10,-3)
    {\includegraphics[width=.1\textwidth]{iconbrowser.png}\\Web Browser};
  \node[inner sep=0pt, align=center] (adblockernotdetected) at (15,0)
    {\includegraphics[width=.1\textwidth]{iconko.png}\\Ad-Blocker detected};
  \draw[->,thick] (browserwithads.east) -- (adblocker.west);
  \draw[->,thick] (adblocker.east) -- (browserwithoutads.west);
  \draw[->,thick] (browserwithoutads.east) -- (adblockerdetector.west);
  \draw[->,thick] (adblockerdetector.230) -- (browser.130)
      node[midway,fill=white] {Ad visible?};
  \draw[->,thick] (browser.50) -- (adblockerdetector.310)
      node[midway,fill=white] {No};
  \draw[->,thick] (adblockerdetector.east) -- (adblockernotdetected.west);
  \end{tikzpicture}
  \end{changemargin}
\end{figure}

\begin{figure}[H]
  \centering
  \caption{\textbf{Sample JavaScript code for an ad-blocker detector based on element hiding rules. The CSS class 'adsbox' is included in the popular \emph{EasyList} filter list, and therefore, elements with this class are hidden by the ad-blocker. The usage of setTimeout ensures that the ad-blocker has enough time to react.}}

  \begin{changemargin}{1cm}{1cm}
  \begin{minted}[linenos,
                 frame=lines]{html}
<script>
window.onload = function() {
    setTimeout(function() {
        var baitElement = document.getElementById("baitElement");
        if (baitElement.clientHeight === 0)
            alert("Ad-Blocker Detected");
        baitElement.parentNode.removeChild(baitElement);
    }, 500);
};
</script>
<div class="adsbox" id='baitElement'>BAIT</div>
  \end{minted}
  \end{changemargin}
\end{figure}

\subsection{State of the art of ad-blocking}

\subsubsection{Anti-adblocking detector software}

\paragraph{AdBlock Plus, uBlock Origin (and similar techniques by ad-blockers)}

\emph{AdBlock Plus} expanded the definition of the AdBlock Plus filter list format to include two new options aimed at avoiding detection by some of the most simple techniques used by ad-blocker detectors~\cite{adblockplusgenerichideblock}. Those new options are supported by most blacklist-based ad-blockers. However, ad-block detectors can easily use other available techniques that are not affected by there new options. \\

\emph{uBlock Origin} includes various lists (under the name "uBlock filters"), which are enabled by default, that include workarounds for generic ad-blocker detectors such as \emph{BlockAdBlock}~\cite{blockadblock} or specific workarounds for popular websites. Additionally, it offers installing anti-ad-blocker detector lists such as those of \emph{AdBlock Protector}~\cite{adblockprotector} or \emph{Anti-AdBlock Killer}~\cite{antiadblockkiller}.

\paragraph{Anti-Adblock Killer}

\emph{Anti-Adblock Killer}~\cite{antiadblockkiller} is a set consisting of a blacklist and a browser add-on (in the form of a user script) that is designed to work along other ad-blockers (such as \emph{AdBlock Plus} or \emph{uBlock Origin}) and which tricks ad-blocker detection scripts and other countermeasures into thinking that no ad-blocker is installed.

Technically, it works by various means (blocking URLs, replacing website scripts, adding elements to the page, etc.), all of which require manually understanding the way each website detects an ad-blocker and implementing a workaround to avoid the detection. As such, it is not a generic anti-ad-blocker detector.

Its source code is available under a Creative Commons BY-SA license~\cite{antiadblockkiller}.

\paragraph{AdBlock Protector}

\emph{AdBlock Protector}~\cite{adblockprotector} is a solution which is technically similar to \emph{Anti-Adblock Killer}.

Its source code is available under the GPL license~\cite{adblockprotector}.

\paragraph{Visual ad-blocker prototype (Perceptual Ad Highlighter)}

\emph{Perceptual Ad Highlighter}~\cite{perceptualadhighlighter} (previously covered) also pioneers a generic anti-ad-blocking detector technique. Unlike most ad-blockers, \emph{Perceptual Ad Highlighter} doesn't completely hide the advertisements from the page, but rather covers them with a layer marking them as advertisements.

In order to avoid detection by ad-blocker detectors, \emph{Perceptual Ad Highlighter} uses the capability of modern web browsers to overwrite the implementation of the APIs used by websites to examine the contents of the website. It uses this technique to prevent the website from being able to enumerate or interact with the layers covering the advertisements. The techniques used by this software are covered further in later sections.

\subsubsection{Research: 'The Future of Ad Blocking'}

During the elaboration of this thesis, a paper called \emph{The Future of Ad Blocking: An Analytical Framework and New Techniques}~\cite{visualadblockerpaper} was published that explores similar techniques to the ones we will explore in this thesis. The following is a brief summary of the techniques explained in the paper:

\begin{itemize}

\item The paper argues that ad-blocking, ad-blocking detection and anti-ad-blocking detection game isn't a never-ending arms race, but rather a situation that ad-blocker users are fated to win.

A simplified explanation of the argument is that the only reason that ad-blocker detectors can exist nowadays is because the current generation of ad-blockers act on the same level as websites (due to working on the same DOM tree accessible to the website's scripts), which renders them detectable. However, they note that since ad-blockers can work at a higher privilege level than websites, it's possible for them to completely hide their changes to the website's DOM tree, rendering them undetectable. Additionally, they note that current browsers already expose certain APIs that make this approach possible.

\item The paper explores using visual detection for advertisement detection, unlike the common mainstream manually maintained blacklist approach. They report that they are able to achieve good accuracy, mostly due to the fact that most advertisements on popular websites have distinguishing tells that make them recognizable.

\begin{figure}[H]
  \centering
  \caption{\textbf{As an example, the AdChoices logo is present on many advertisements on popular websites, which makes them easily detectable by visual recognition}}
  \includegraphics[width=0.15\linewidth]{adchoices.png}
\end{figure}

\item The paper explores avoiding detection by ad-blocker detector scripts by using APIs implemented in most modern browsers (Firefox, Chrome,Edge, etc.), that allow overwriting the browser's native DOM properties with user-defined functions.

The author's approach is to make part of the DOM inaccessible to the website scripts. In this hidden part of the DOM, elements overlaying the advertisements are placed in order to mask the ads.

This effectively avoids detection by ad-blocker detectors, since it creates a 'safe' area in the DOM where the elements overlaying the advertisements can be placed, without the website being able to detect them as they are unable to access this area of the DOM.

\begin{figure}[H]
  \centering
  \caption{\textbf{Model presented by the researchers. They separate the website's DOM ("Old DOM root") from the part of the DOM being used to mask the advertisements ("Overlay root") and make it inaccessible by the website's scripts. Source: 'The Future of Ad Blocking'}}
  \includegraphics[width=0.8\linewidth]{thefutureofadblockingoverlaydom.png}
\end{figure}

\item Due to its visual approach, the paper doesn't outline how to maintain the privacy (tracking), data usage and performance considerations. Since the approach of the paper doesn't rely on filter lists but rather on visual detection of advertisements, those issues can't be properly addressed, because a tracking script can be implemented without showing anything visually, and the advertisement resources should be downloaded for visual detection to happen.

\item The paper presents a prototype implementation of their techniques as a Chrome browser extension, called \emph{Perceptual Ad Highlighter}~\cite{perceptualadhighlighter}.

\end{itemize}

\subsection{Motivation and objectives of the thesis}

\subsubsection{Objective}

In this paper, we propose modifications to blacklist-based ad-blockers, that add the capability to avoid detection by ad-blocking detectors in a generic way that doesn't require manual workarounds for each website (unlike software like \emph{Anti Ad-Block Killer}~\cite{antiadblockkiller}). \\

In comparison with similar techniques presented in \emph{The Future of Ad Blocking}~\cite{visualadblockerpaper}, we show that those can be implemented in a way that allows removing the space used by the advertisements in the page, and compromising to a lesser degree the privacy-enhancing and data-usage features of ad-blockers.

\subsubsection{Motivation}

\paragraph{Educational}

It is of research interest to study the principles under which Internet advertising, ad-blockers and ad-blocker detectors work, and which are the ways they can be avoided, in order to be able to provide accurate information to users of the implications of those technologies.

\paragraph{Website Compatibility}

In most circumstances, it is desirable to avoid website scripts from being able to detect the changes produced by an ad-blocker, because this minimizes the risk of the website scripts malfunctioning in the presence of an ad-blocker.

In addition, the techniques we use to avoid detection by ad-blockers can also be used to implement arbitrary aesthetic changes to websites without breaking compatibility with the website scripts.

\paragraph{Malvertising and privacy risks}

Since advertisements are often used as a vector for distributing malware~\cite{malvertising}, in some environments it may be not be considered desirable to disable ad-blockers in order to access certain websites, in order to maintain protection against malware and privacy risks.

\paragraph{Avoiding annoyances and content restrictions}

Like advertisements, ad-blocker detectors and its associated behaviors (prompts to disable ad-blockers, avoiding access to applications or websites, etc.) are often considered an annoyance by users. Users who have a clear stance about advertising (e.g. only acceptable ads, or no advertisements at all), may wish to avoid those restrictions to the maximum possible extent.

\paragraph{Implementing "Acceptable ads" and ad replacement plans}

Ad-blocker detection and its associated behaviors may hinder adoption of alternate advertisement policies such as "Acceptable ads"~\cite{adblockaceptableads} or ad-replacement policies~\cite{bravebrowser} by discouraging users to use ad-blockers, therefore impacting the adoption of measures that may improve the overall Internet advertising experience for users.

\paragraph{Fingerprinting}

Browser fingerprinting is a method of tracking web browsers that relies on identifying an internet user by the settings and unique characteristics of his browser, such as the browser, operating system, fonts, language, plugins, etc., (a "fingerprint") instead of traditional methods such as cookies, local storage, IP addresses, etc.~\cite{panopticlickbrowserfingerprinting}~\cite{chromiumfingerprinting}. \\

Unlike the traditional methods of tracking, which are mostly transient and easily cleaned by standard browser utilities, it is very difficult to defend against fingerprinting, both because it's difficult to detect when it's being used, and because the characteristics of the fingerprint are hard to change.

Whether or not a user uses an ad-blocker can be used as a characteristic of the fingerprint (as detected by an ad-blocker detector). In addition, in the annexes (see~\ref{subsec:browserfingerprintingbyfilterlists}), we show that it's possible to detect which filter lists an user has enabled, which creates a much more accurate and persistent fingerprint (multiple bits of fingerprint information instead of a single bit). Therefore, it is of interest to forbid detection by ad-blocker detectors in order to improve online privacy.

\paragraph{Tracing / Statistics}

While not the main aim of this thesis, the techniques we present to detect when an advertiser script is being executed and when it accesses privacy-sensitive browser APIs can be used in order to gather information about the privacy consequences of those scripts without requiring manual analysis or reverse engineering.

\paragraph{Blocking of advertisements in locked-down contexts}

Our implementation of an ad-blocker as a main-in-the-middle proxy is technically capable, unlike other ad-blockers, of blocking advertisements in browsers and mobile applications that are traditionally not blocked by network-level solutions such as \emph{Pi-Hole}~\cite{pihole} that only work at the network-level by blocking access to certain hosts.

While similar implementations to our ad-blocker prototype in this sense exist, we have not found any open source implementation that work in such manner.

\subsubsection{Overview of the techniques}

We propose the following two techniques that could be added to existing blacklist-based ad-blockers, which enable reliable generic ad-block detector avoidance:

\begin{itemize}

\item \textbf{Website script context detection:} We examine how to implement a way for ad-blockers to receive a callback when a script in a website is entered or left. This can be used by ad-blockers (or any similar program, such as other browser extensions), to restore the website to the state ad-blocker detectors expect when executing (for example, virtually redisplaying the advertisements when a website script is entered, and hiding them after the website script execution finishes).

This technique is mostly concerned with avoiding detection by ad-blocker detectors that work by abusing \textbf{element hiding rules}.

This is accomplished by either automatically inserting hooks to the contents of the website scripts, or by a technique based on overwriting certain DOM properties.

Unlike in \emph{The Future of Ad Blocking: An Analytical Framework and New Techniques}~\cite{visualadblockerpaper}, this method doesn't require hiding part of the DOM, but rather allows the ad-blocker to work in a way that is more similar to the way current mainstream ad-blockers work (which don't add overlay elements to mask the ads, but rather completely hide the ads, visually removing them from the website layout).

\item \textbf{Advertiser script context detection:} In order to avoid detection by those ad-blocker detectors based on \textbf{address blocking rules} in a generic way, we necessarily have to allow the advertiser's resources to be downloaded and executed in order to effectively avoid detection by ad-blocker detectors, since otherwise it's trivial to build an ad-blocker detector using a sufficiently complex script. Therefore, we examine how to allow those scripts to execute while simultaneously avoiding access to privacy-sensitive APIs (e.g. cookies, location, mouse position, etc.) by advertiser scripts (i.e. those scripts that are filtered by filter lists). However, regular website scripts should be able to access those APIs normally, since websites typically depend on those for proper function (e.g. cookies for persistent authentication to websites, location for maps applications, etc.)

We purpose two approaches, which are analogous to the ones used for website script context detection. One approach is modifying the advertiser scripts to insert hooks in order to receive a callback when a advertiser script is entered or left. Another approach is detecting when an advertiser script accesses a privacy-sensitive browser API by overwriting it (similarly to overwriting certain DOM properties) and using non-standard but widely supported browser APIs in order to walk the call stack and detect if a advertiser script has caused the call to occur or not.

When an advertiser script accesses a privacy-sensitive API, we can present a 'virtualized' version of the API that avoids exposing sensitive information. For example, the cookies set by the advertiser script can be non-permanent and cleaned when the user exits the website, or the access to location APIs can return falsified information.

\end{itemize}

However, it's worth noting that, when implementing \textbf{advertiser script context detection}, we reintroduce the \textbf{additional data usage} caused by downloading the resources required by the advertisements, which in classical blacklist-based ad-blockers are blocked. In addition, as we will see, applying those two techniques will have a non-negligible performance impact. \\

To mitigate those problems, we propose (but don't implement in our prototype) a third modification to blacklist-based ad-blockers. This modification would be to add an additional filter option in filter lists, which would determine whether to apply the proposed techniques. By default, an ad-blocker would not apply the proposed techniques, and therefore would work like the contemporary ad-blockers. However, if such a filter option were present, the anti-ad-blocker detection techniques would be applied.

Both under the observation that nowadays, only a relatively small proportion of websites employ ad-blocker detectors, and the game-theoretical observations in \emph{The Future of Ad-Blocking}~\cite{visualadblockerpaper} (publishers and advertisers have no incentive to use ad-blocker detectors if effective anti-ad-blocker detectors are available), we expect that such an option would effectively balance the desire to avoid additional data usage with the desire to use anti-ad-blocker detectors. \\

It's also worth noting that we have implemented the proposed ad-blocker as regular scripts which are inserted into the website \textbf{by a man-in-the-middle proxy} (similarly to \emph{AdGuard} or \emph{AdFender}), instead of a browser extension (similarly to \emph{AdBlock Plus} or \emph{uBlock Origin}).

This has the advantage of browser independence, requiring only a setup for the whole network, and allowing the implementation to work even in "locked-down" contexts such as mobile browsers or mobile apps. Due to the increasing prevalence of those contexts, this is an approach to implementing ad-blockers that is increasingly valuable.

However, we also note that this is an implementation detail is orthogonal to all other features of ad-blockers (i.e. any ad-blocker can be implemented in this way), and therefore isn't an essential part of our prototype.

\subsubsection{Technical comparison to existing ad-blocking software}

The following table summarizes the capacity of the various ad-blockers in the market and research prototypes, according to the explained balances of features:

\begin{figure}[H]
  \centering
  \caption{\textbf{Our interpretation of the various relative strenghts and weaknesses of ad-blockers, according to various criteria}}
    {\footnotesize
    \begin{changemargin}{-0.5cm}{-0.5cm}
    \begin{center}
        \begin{tabular}{| l | l | l | l | l | l | l |}
        \hline
        \textbf{Ad-Blocker} & \textbf{Blocks ads} & \textbf{Privacy} & \textbf{Undetectable} & \textbf{Data Usage} & \textbf{Reliable} & \textbf{Mantainance} \\ \hline
        \textbf{(No ad-blocker)} & \tno & \tno & \tok N/A & \tno & \tok N/A & \tok None \\ \hline
        \multicolumn{7}{|c|}{Group 1 (Classic blacklist-based)} \\ \hline
        \textbf{AdBlock Plus} & \tyes & \tyes & \tno & \tyes & \tyes & \twrnhi Filter lists \\ \hline
        \textbf{uBlock Origin} & \tyes & \tyes & \tno & \tyes & \tyes & \twrnhi Filter lists \\ \hline
        \textbf{Brave Browser} & \tyes & \tyes & \tno & \tyes & \tyes & \twrnhi Filter lists \\ \hline
        \textbf{Pi-Hole} & \twrnhi Only 3rd-party & \tyes & \tno & \tyes & \tyes & \twrn Host lists \\ \hline
        \textbf{AdGuard} & \tyes & \tyes & \tno & \tyes & \tyes & \twrnhi Filter lists \\ \hline
        \textbf{AdFender} & \tyes & \tyes & \tno & \tyes & \tyes & \twrnhi Filter lists \\ \hline
        \multicolumn{7}{|c|}{Group 2 (Classic anti-ad-blocker detectors along with a supported ad-blocker)} \\ \hline
        \textbf{Anti-Adblock Killer} & \tyes & \tyes & \tyes & \tyes & \tyes & \tko \specialcell{Filter lists + \\ detector fixes} \\ \hline
        \textbf{AdBlock Protector} & \tyes & \tyes & \tyes & \tyes & \tyes & \tko \specialcell{Filter lists + \\ detector fixes} \\ \hline
        \multicolumn{7}{|c|}{Group 3 (Heuristic-based)} \\ \hline
        \textbf{Privacy Badger} & \twrnhi Only 3rd-party & \tyes & \tno & \tyes & \tno & \tok None \\ \hline
        \textbf{Perceptual ad hi.} & \tyes & \tno & \tyes & \tno & \twrn Mostly yes & \tok Visual acc. \\ \hline
        \multicolumn{7}{|c|}{Group 4 (Presented prototype)} \\ \hline
        \textbf{Content-Filter-Policy} & \tyes & \tok \specialcell{Yes\\ (Ideally)} & \tyes & \twrnhi Mostly no & \tyes & \twrnhi \specialcell{Filter lists} \\ \hline
        \multicolumn{7}{|c|}{Group 5 (Technically possible implementation with improved filter lists)} \\ \hline
        \textbf{(Not implemented)} & \tyes & \tyes & \tyes & \twrn Mostly yes & \tyes & \twrnhi \specialcell{Filter lists + \\ undetectable option} \\ \hline
        %\multicolumn{7}{|c|}{(Implementation combining \textbf{Perceptual ad highlighter} and \textbf{Content-Filter-Policy})} \\ \hline
        %\specialcell{\textbf{Perceptual ad hi. +} \\ \textbf{Content-Filter-Policy}} & \tyes & \tyes & \tyes & \twrnhi Mostly no & \twrn Mostly yes & \twrn \specialcell{Visual acc. +\\ Host lists \\ / heuristics} \\ \hline
        \end{tabular}
    \end{center}
    \end{changemargin}
    }
\end{figure}

On the first group, there are the \textbf{classical blacklist-based ad-blockers}, which work by simply blocking addresses/elements according to blocking rules. Those can generally block all kinds of advertisements, improve privacy and reduce data usage. However, due to their implementation, they can be easily detected by ad-blocker detectors, and require manual maintenance of the associated filter lists.

On the second group are the \textbf{ad-blockers from the previous group, combined with a classic anti-ad-blocker detector} based on manual workarounds. Those add protection from ad-blocking detectors, at the cost of adding further maintenance costs.

On the third group are the \textbf{experimental heuristic blockers}. \emph{Privacy Badger}, due to its heuristic approach to detecting third-parties, isn't too reliable and can't block all kinds of advertisements, in addition to not having any anti-ad-blocker detector techniques. \emph{Perceptual Ad Highlighter} uses visual detection to detect advertisements, but due to its approach, it can't address the privacy and data usage problems, and while it can reliably block most advertisements, is constrained by the accuracy of the visual detection.

On the fourth group is \textbf{our proposal}, which is a modification to the classical blacklist-based ad-blockers that adds anti-ad-blocker detector techniques. This comes at the cost of incremented data usage, but doesn't add any additional maintenance costs to blacklist-based ad-blockers. Completely locking down access to privacy-sensitive APIs requires work due to their extent, but is possible with enough effort.

On the fifth group is \textbf{a technically possible (but not implemented) proposal}, which is a modification of our proposal that aims to improve data usage. Under this proposal, a filter option would be added to filter lists that would specify whether to block advertiser scripts (necessary for data usage) or allow them in a restricted environment (necessary for avoiding detection by ad-blocker detectors). This would address data usage concerns with a negligible impact on filter list maintenance.
