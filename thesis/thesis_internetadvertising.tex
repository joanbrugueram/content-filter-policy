\section{Context of advertisement blocking}
\label{sec:thesis_internetadvertising}

\subsection{Internet advertising}

Internet advertising refers to the inclusion of advertising material of any kind, such as text, images, videos, etc., into resource accessible from the Internet, such as websites, emails, apps, etc.

\begin{changemargin}{-0.5cm}{-0.5cm}
\begin{multicols}{2}

\begin{figure}[H]
  \centering
  \caption{\textbf{Advertisements on a desktop website.} Source: \emph{WordReference}}
  \includegraphics[width=\linewidth]{adswebsite.png}
\end{figure}

\columnbreak

\begin{figure}[H]
  \centering
  \caption{\textbf{Advertisements on a mobile app.} Source: \emph{Duolingo}}
  \includegraphics[width=0.6\linewidth]{adsapp.png}
 \end{figure}

\end{multicols}
\end{changemargin}

Internet advertising is used on a notable proportion of Internet resources, mainly because it provides a revenue source for websites.

\subsubsection{Basic Internet advertising terminology}

An internet advertisement is typically called an \textbf{ad}, for short. \\

In the context of Internet advertising, the party that controls the Internet resource where the advertisement is shown is called the \textbf{publisher} (such as a newspaper, a social network, etc.). The party that controls the product being advertised is called the \textbf{advertiser} (such as a clothing brand, a supermarket, etc.).

The scheme where the publisher is directly in contact with advertisers, and thus serves the advertisements directly from his website, is called \textbf{first-party advertising}.

\begin{figure}[H]
  \centering
  \caption{\textbf{First-party advertising (Simplified)}}
  \begin{tikzpicture}[thick,scale=0.85, every node/.style={transform shape}]
  \node[inner sep=0pt, align=center] (user) at (0,0)
    {\includegraphics[width=.1\textwidth]{iconuser.png}\\User};
  \node[inner sep=0pt, align=center] (browser) at (2.5,0)
    {\includegraphics[width=.1\textwidth]{iconbrowser.png}\\Browser, app, etc.};
  \node[inner sep=0pt, align=center] (firstpartyserver) at (6,0)
    {\includegraphics[width=.1\textwidth]{iconserver.png}\\First party server\\www.website.com};
  \node[inner sep=0pt, align=center] (firstpartycontent) at (10,0)
    {\includegraphics[width=.1\textwidth]{iconhtml.png}\\Website content};
  \node[inner sep=0pt, align=center] (firstpartyimages) at (10,-2)
    {\includegraphics[width=.1\textwidth]{iconimage.png}\\Website images};
  \node[inner sep=0pt, align=center] (firstpartyadvertisements) at (10,-4)
    {\includegraphics[width=.1\textwidth]{iconjs.png}\\Advertisements};
  \draw[->,thick] (user.east) -- (browser.west);
  \draw[->,thick] (browser.east) -- (firstpartyserver.west);
  \draw[->,thick] (firstpartyserver.east) -- (firstpartycontent.west);
  \draw[->,thick] (firstpartyserver.east) -- (firstpartyimages.west);
  \draw[->,thick] (firstpartyserver.east) -- (firstpartyadvertisements.west);
  \end{tikzpicture}
\end{figure}

Otherwise, the scheme where the publisher is not directly in contact with the advertisers, but instead relies on an \textbf{advertising network} (such as Google AdSense, AdBlade, etc.) to contact with advertisers and serve advertisements, is called \textbf{third-party advertising}.

\begin{figure}[H]
  \centering
  \caption{\textbf{Third-party advertising (Simplified)}}
  \begin{tikzpicture}[thick,scale=0.85, every node/.style={transform shape}]
  \node[inner sep=0pt, align=center] (user) at (0,0)
    {\includegraphics[width=.1\textwidth]{iconuser.png}\\User};
  \node[inner sep=0pt, align=center] (browser) at (2.5,0)
    {\includegraphics[width=.1\textwidth]{iconbrowser.png}\\Browser, app, etc.};
  \node[inner sep=0pt, align=center] (firstpartyserver) at (6,0)
    {\includegraphics[width=.1\textwidth]{iconserver.png}\\First party server\\www.website.com};
  \node[inner sep=0pt, align=center] (firstpartycontent) at (10,0)
    {\includegraphics[width=.1\textwidth]{iconhtml.png}\\Website content};
  \node[inner sep=0pt, align=center] (firstpartyimages) at (10,-2)
    {\includegraphics[width=.1\textwidth]{iconimage.png}\\Website images};
  \node[inner sep=0pt, align=center] (thirdpartyserver) at (6,-4)
    {\includegraphics[width=.1\textwidth]{iconserver.png}\\Third party server\\www.advertiser.com};
  \node[inner sep=0pt, align=center] (thirdpartyadvertisements) at (10,-4)
    {\includegraphics[width=.1\textwidth]{iconjs.png}\\Advertisements};
  \draw[->,thick] (user.east) -- (browser.west);
  \draw[->,thick] (browser.east) -- (firstpartyserver.west);
  \draw[->,thick] (browser.east) -- (thirdpartyserver.west);
  \draw[->,thick] (firstpartyserver.east) -- (firstpartycontent.west);
  \draw[->,thick] (firstpartyserver.east) -- (firstpartyimages.west);
  \draw[->,thick] (thirdpartyserver.east) -- (thirdpartyadvertisements.west);
  \end{tikzpicture}
\end{figure}

Nowadays, the majority of websites rely on third-party advertising via advertising networks, since it avoids having to contact multiple advertisers and simplifies the integration process.

Advertising networks typically provide additional tools. \textbf{Tracking} refers to obtaining and storing additional user data, such as relevant interests, browser history, social media profiles, etc.. Tracking enables \textbf{targeted advertising} (showing ads based on user data, in order to improve relevancy) and \textbf{analytics} (statistical information accessible to publishers about its users).

\subsection{Internet advertisement blocking}

Internet advertisement blocking refers to any software techniques that are aimed at removing advertisements from web sites and reducing their associated consequences (such as tracking).  \\

Some basic techniques in this direction are already implemented by default on mainstream browsers (such as \emph{Google Chrome}, \emph{Mozilla Firefox}, \emph{Internet Explorer}, etc.). Examples are pop-up blockers~\cite{chromepopupblockers}, click to play for plugins~\cite{firefoxclicktoplay}, do-not-track indicators~\cite{internetexplorerdonottrack}, tracking protection~\cite{firefoxtrackingprotection}, etc. However, no mainstream browser implements generic ad-blocking functionality by default. Instead, users must download a separate program or browser add-on, after the installation of which advertisements no longer display on the websites they visit (an "ad-blocker"). \\

Internet advertisement blocking has been soaring in the recent years, and according to various reports by advertising industry members~\cite{thecostofadblocking}~\cite{pagefair2017adblockreport}, ad-blockers were used by hundreds of millions of Internet users, causing multi-billion dollar losses according to the advertising industry.

\subsubsection{Perceived benefits of ad-blocking}

Among the benefits perceived by users of using an advertisement blocker while browsing online, there are~\cite{adblockplususersurvey}:

\begin{itemize}

\item \textbf{Avoiding irrelevant content:} By blocking advertisements, users can be often able to locate what they are looking for faster in a website, by reducing the amount of visual clutter and content not directly related to their target.

\item \textbf{Avoiding interruptions and distractions:} Often, advertisements are placed or designed in such a way that they interrupt or otherwise distract the user in a way that impedes him to effectively do their objective. Examples of this are~\cite{adblockaceptableads}: Excessively big advertisements, advertisements mixed with the content, \emph{pop-ups}~\cite{webopedia} and \emph{pop-unders}~\cite{webopedia}, animated advertisements (such as GIFs or videos), advertisements with sound, interstitial page ads, advertisements disguising as content, pornographic or otherwise inappropriate advertisements, etc.

\end{itemize}

\begin{changemargin}{-0.5cm}{-0.5cm}
\begin{multicols}{2}

\begin{figure}[H]
  \centering
  \caption{\textbf{Interstitial advertisement:} The user is forced to watch an advertisement and wait before being able to see any content on the website. Source: \emph{Forbes.com}}
  \includegraphics[width=0.9\linewidth]{interstitialads.png}
\end{figure}

\columnbreak

\begin{figure}[H]
  \centering
  \caption{\textbf{Misleading advertisements:} Download buttons for other products before starting a software download. Source: \emph{Softonic.com}}
  \includegraphics[width=\linewidth]{misleadingads.png}
 \end{figure}

\end{multicols}
\end{changemargin}

\begin{itemize}

\item \textbf{Reduced data usage:} If advertisements are blocked, they do not need to be downloaded, and therefore, substantially less bandwidth is used~\cite{advertisementsdatausage}. This is specially important in metered connections, such as most mobile networks.

\item \textbf{Increased performance and reduced power use:} If advertisements are blocked, no additional computing power needs to be used to download and display them. This can make websites load faster and reduce power use. This is specially important in mobile devices, which are usually limited by battery capacity.

\item \textbf{Increased privacy:} It has been known that multiple advertising networks attempt to gather and store additional user data in order to do targeted advertising or sell it to other third parties. Examples include: Browser history (cross-website tracking), physical location, IP address, social media profiles, unique user identifiers (such as cookies, local storage, or other 'supercookie'/'fingerprinting' techniques~\cite{supercookiestrackyou}), cross-device tracking, etc.

\item \textbf{Increased security:} Malware authors have often used advertisements as a way to distribute malware ("malvertising")~\cite{malvertising}, either by tricking the user into downloading malware programs, or by exploiting browser vulnerabilities in a way that does only require the user to navigate to an otherwise trusted source.

\item \textbf{Ideological reasons:} Users may wish to block advertisements for ideological reasons, such as avoiding consumerism or avoiding financing inappropriate websites they may incidentally visit (such as hate groups).

\end{itemize}

\subsubsection{Perceived drawbacks of ad-blocking}

On the other hand, various publishers and advertisers maintain that advertisements have the following benefits:

\begin{itemize}

\item \textbf{Revenue source for websites:} Many websites have in-page advertisements as their main or only revenue source. By blocking advertisements, users can decrease the revenue obtained by the website, forcing it to find additional revenue sources (subscriber-only content, crowdfunding, donations, etc.) or closing the website, therefore ultimately reducing the amount of free sources available to users.

\item \textbf{Advertisements can be relevant:} Some advertisers maintain that their advertisements help users find new products or better offers than they may have found otherwise, therefore benefiting the user.

\end{itemize}

\subsection{Reactions to Internet advertisement blocking}

\subsubsection{"Acceptable Ads" and similar initiatives}

Due to the concerns outlines above, as well as the desire of some ad-blocking software authors to monetize their software, some of the authors of ad-blocking software have started to implement initiatives that aim to find a balance between blocking all advertisements and allowing all forms of advertising, no matter how intrusive it is:

\begin{itemize}

\item The ad-blocker software company Eyeo GmbH (the owner of the ad-blocking software \emph{Adblock Plus}) has started an initiative called "Acceptable Ads"~\cite{adblockaceptableads}~\cite{adblockaceptableads}, which aims to reduce intrusive advertising by defining a set of visual guidelines (placement, size and distinction) that advertisements must follow in order to be displayed. Other restrictions in matters such as tracking or data usage are enforced.

Under this model, advertisements are manually allowed ("whitelisted") by the company behind the ad-blocking software after publishers request and prove their advertisements follow those guidelines. Larger publishers must give the ad-blocking company software and its partners a share of the revenue generating by the advertisements~\cite{adblockaceptableads}~\cite{acceptableadsmoney}. This has attracted criticism by members of the advertising industry~\cite{acceptableadsmoney} as well as some groups of users~\cite{therearenoacceptableads}.

"Acceptable ads" are allowed by default by \emph{Adblock Plus}, but users can opt-out of the initiative and block all advertisements without any penalty.

\item Brave software, the company behind the ad-blocking \emph{Brave browser}, has started an "ad-replacement system"~\cite{braveadreplacement} that replaces the blocked advertisements by alternative advertisements served by the browser, which they define as faster, safer, and more private~\cite{braveadreplacement}.

In this system, the revenue generated by the advertisements is split between the users, publishers and the company and its advertisement partners~\cite{braveadreplacement}. Publishers must opt-in to the system in order to receive their share. This system has been criticized and is being legally challenged by some publishers~\cite{bravepublishersstrikeback}.

\item The authors of other ad-blocking software such as \emph{uBlock Origin} have stated that they have no interest in allowing any advertisement in any form whatsoever by default~\cite{ublockoriginnoacceptableads}.

\end{itemize}

\subsubsection{Google Contributor}

\emph{Google Contributor}~\cite{googlecontributor} is a program launched by Google offered to remove advertisements from selected websites in the Google advertising platform~\cite{googlecontributorarticle}. \\

The program asks Google users to pay a fee (from 1\$ to 3\$ per month, as decided by the user) to remove ads from the selected websites. Google then pays the selected publishers using the fees paid by the users instead of the revenue generated by the advertisements.

The program launched in late 2014, but was shut down in early 2017, citing a replacement coming in 2017. The replacement program was finally launched on June 2017.~\cite{googlecontributor}. \\

\subsubsection{Ad-blocker detectors}

Due to the usage of ad-blockers going mainstream (most sources estimate they are used by approximately 10\% to 20\% of users), many websites have started implementing scripts that detect whether or not an user has an ad-blocker installed and change their behavior according to the results. Various behavior have been attempted, including:

\begin{itemize}

\item Blocking access to the user to the website until the ad-blocker is disabled.

\item Asking the user to whitelist the website from its ad-blocker.

\item Warning the user that the website depends on advertising revenue, but letting the user continue without further consequences.

\item Asking the user to buy a paid subscription to the website.

\item Asking the user to help finance the website by using another method (such as donations or crowdfunding).

\end{itemize}

Those have been met with various degrees of success. Some case studies seem to indicate that blocking access to users until they disable their ad-blockers indeed results in a notable percentage of users disabling it~\cite{forbesadblockingplanresults}~\cite{sitesthatblockadblockerssuffer}, while another notable percentage of users abandon the website.

\begin{changemargin}{-0.5cm}{-0.5cm}
\begin{multicols}{2}

\begin{figure}[H]
  \centering
  \caption{\textbf{Ad-blocker detector which blocks users from the website.} Source: \emph{Diario de Navarra}}
  \includegraphics[width=\linewidth]{adblockdetectorblocking.png}
\end{figure}

\columnbreak

\begin{figure}[H]
  \centering
  \caption{\textbf{Ad-blocker detector which warns users without blocking them from the website.} Source: \emph{Splitwise}}
  \includegraphics[width=0.6\linewidth]{adblockdetectornonblocking.png}
 \end{figure}

\end{multicols}
\end{changemargin}

There exist a large amount of ad-blocker detectors, from pre-made scripts such as \emph{BlockAdBlock}~\cite{blockadblock} to custom solutions developer by publishers. \\

In the European Union, there was initially some controversy about the legality of using ad-blocker detectors without explicit user consent following the interpretation of the privacy restrictions applied to websites within the European Union~\cite{antiadblockerslegalchallenges}, but this issue has been settled by the European Union's governing body which has explicitly noted ad-blocker detectors are legal~\cite{theeuisfinewithblockingadblockers}.

\subsection{Ad-Blocking Software}

Here, we explain the main approaches used by ad-blocking software. \\

Technically, most of the software we refer to as "ad-blockers" is really "content blocking software" - it can be used for purposes other than blocking advertisements, such as blocking malware, inappropriate (pornographic, etc.) content, tracking, social media, etc., without necessarily blocking advertisements. However, we will refer to them as ad-blockers for brevity and in reference to their most common use, blockign advertisements

\subsubsection{Blacklist-based ad-blockers}

This kind of ad-blockers defined in this list are based on using a list which defines a set of blocking rules (elements and web addresses that should be blocked or hidden when accessing a website). Typically, those lists are manually maintained by volunteers, such as EasyList~\cite{easylist} (explained below).

\paragraph{AdBlock Plus}

\emph{AdBlock Plus}~\cite{adblockplus} is an ad-blocker that is available for various platforms and browsers (e.g. \emph{Mozilla Firefox}, \emph{Google Chrome}, \emph{Microsoft Edge}, \emph{Safari}). It was one of the earliest ad-blockers available and is currently the overall most popular ad-blocker add-on~\cite{firefoxaddonsmostpopular}~\cite{chromewebstore}~\cite{adblockplus}.

Its source code is freely available under the GPL license. Its authors operate a company called EyeO GmbH on Germany who is behind the "Acceptable ads" initiative~\cite{adblockaceptableads}.

\paragraph{uBlock Origin}

\emph{uBlock Origin}~\cite{ublockorigin} (also known as \emph{$\mu$Block Origin}, formerly \emph{uBlock}) is an ad-blocker that is delivered as a browser add-on, with wide browser support and similar technical principles to \emph{AdBlock Plus}. It is also one of the most popular ad-blocker add-ons~\cite{firefoxaddonsmostpopular}~\cite{chromewebstore}.

Its source code is freely available under the GPL license. Its author, gorhill (Raymond Hill), does not economically benefit from the project and believes that all advertisements should be blocked with no exceptions~\cite{ublockoriginnoacceptableads}. \emph{uBlock Origin} is focused on privacy and performance, with some benchmarks considering it the most performant ad-blocker~\cite{ublockoriginperformancefirstparty}~\cite{ublockoriginperformancethirdparty}.

\paragraph{Brave Browser}

\emph{Brave Browser}~\cite{bravebrowser} is a browser with an integrated ad-blocker. It has taken a novel initiative by creating an "ad-replacement system"~\cite{braveadreplacement} that replaces web advertisements with advertisements that are served by the browser, and which are defined by their authors as faster, safer, and more private~\cite{braveadreplacement}. The company offers sharing a portion of the revenue generated by the advertisements with the publishers. This system is being legally challenged by some publishers~\cite{bravepublishersstrikeback}.

Its source code is available under the Mozilla Public License.

\paragraph{Pi-Hole}

\emph{Pi-Hole}~\cite{pihole} is a network-wide ad-blocker, which is independent of any browser and operating system. It works by creating a DNS server that disallows communication with the domains of third-party advertisers. It is designed to be available to all computers of a network without the need to individually configure each device in it.

The list of blocked third-party advertisers is manually maintained. Due to working at a network level, it has less blocking power than browser add-ons and, for example, is unable to block most first-party advertisements.

Its source code is available under the European Union Public License.

\paragraph{AdGuard}

\emph{AdGuard}~\cite{adguard} is a commercial ad-blocker that is available for multiple platforms (iOS, Android, Windows, etc.). \emph{AdGuard for Windows} is capable of working as a man-in-the-middle proxy (a network filter that allows actively intercepting and modifying the content of web requests). This allows \emph{AdGuard for Windows} to work in all programs installed in the computer (all browsers and even desktop applications) without requiring additional configuration for each program, and, unlike DNS solutions like \emph{Pi-Hole}, it maintains the same blocking power as browser add-on based ad-blockers.

Other versions of \emph{AdGuard} for other platforms may have different feature sets according to the platform's limitations. \emph{AdGuard for Windows} is a closed-source program.

\paragraph{AdFender}

\emph{AdGuard}~\cite{adfender} is a commercial ad-blocker that is available for Windows. \emph{AdFender} is technically similar to \emph{AdGuard for Windows}. \emph{AdFender} is a closed-source program.

\subsubsection{Heuristic-based ad-blockers}

\paragraph{Privacy Badger}

\emph{Privacy Badger}~\cite{privacybadger} is a experimental browser add-on focused on blocking third-party advertisers and trackers.

Unlike most ad-blockers which work based on manually defined blocking rules, it automatically detects third-party advertisers and trackers by using an heuristic based on multiple websites referencing the same domain, and thus does not require any manually maintained blacklist. However, due to its heuristic approach, it can easily have false positives (block legitimate domains, such as CDNs), and false negatives (such as advertisers/trackers seen for the first time)~\cite{privacybadgerdeteriorate}.

Its source code is available under the GPL license.

\paragraph{Visual ad-blocker prototype (Perceptual Ad Highlighter)}

\emph{Perceptual Ad Highlighter}~\cite{perceptualadhighlighter} is a prototype ad-blocker based on visual recognition has been developed by a team of Princeton and Stanford University researchers~\cite{visualadblocker}~\cite{visualadblockerpaper}. This kind of ad-blocker doesn't require a manually defined blocking list, but rather visually scans the website looking for content that looks like advertisements. Due to legal requirements on advertising, most advertisements can be easily detected by such a ad-blocker, according to the authors.

Such kind of ad-blocker is in early stages of development and it remains to be seen whether it can be made as reliable as filter list-based blockers. In its current state, it also doesn't address privacy, data usage, or performance related concerns.

\subsubsection{Whitelist-based ad-blockers}

Another class of ad-blockers are those that require the user to define a list of allowed content. Examples of such class of ad-blockers are NoScript~\cite{noscript} and uMatrix~\cite{umatrixrepo}.

This kind of ad-blockers generally disallows the download and execution of all kind of content such as images, scripts, etc., and requires the user to manually specify which content is allowed in the browser. This requires effort and technical knowledge on part of the user to define the allowed content, since very often additional content is required for websites to be functional, but its proponents believe the security, data usage and privacy benefits outweigh the disadvantages.

While generally this kind of blockers are not tailored to blocking advertisements, due to their operation method, they block the majority of advertisements and tracking methods by default.
