\section{Limitations}
\label{sec:thesis_limitations}

\subsection{Browser support}

We have tested our prototype with the \emph{Google Chrome} and \emph{Mozilla Firefox} web browsers, which are the most popular open-source browsers and the browsers that currently have the highest degree of support for the HTML5 standard.

Almost all the code for our prototype follows the currently defined HTML, CSS and JavaScript standards. However, some of the features we have used such as \emph{CSS variables} are currently not supported by all mainstream browsers, despite being already standardized.

Additionally, some notable exceptions are the usage of deprecated \emph{mutation events}~\cite{mozilladevelopernetwork} and the non-standard \mintinline[breaklines]{javascript}{Error.prototype.stack} property~\cite{mozilladevelopernetwork}, which could cause problems with support for other browsers or new versions of the currently supported browsers.

However, all of those problems can be worked around with some implementation effort, so our techniques remain portable to other modern browsers.

\subsection{Detection of the prototype through its 'footprint'}

While our ad-blocker prototype makes ad-blocker detectors unable to detect our ad-blocker through the current techniques (element hiding rules or address blocking rules), future ad-blocker detectors may try to detect our prototype through the 'footprint' that our ad-blocker detector leaves: Overwritten browser APIs, additional scripts inserted into the page, etc.. However, all of those problems were solved in \emph{The Future of Ad-Blocking}~\cite{visualadblockerpaper}:

\begin{itemize}

\item The fact that \textbf{native browser APIs have been overwritten} can be detected by the website by using the \mintinline[breaklines]{javascript}{Function.prototype.toString} function. However, using the 'rootkit-style' techniques in \emph{The Future of Ad-Blocking}, this function can also be overwritten, hiding both the fact that browser APIs have been overwritten and that this same function has been overwritten, providing an 'airtight' system that prevents websites from detecting the ad-blocker~\cite{visualadblockerpaper}.

\item The fact that we \textbf{inject extra scripts and style sheets into the page} (and their associated DOM elements) can be detected by the website using the browser APIs that allow the website to walk the DOM. One can prevent this in two ways:

The simplest is implementing the ad-blocker as a browser extension, which allows it to work in a separate environment as the rest of the website. In this way, making the ad-blocker resources inaccessible to the website is already handled by the existing browser APIs.

An alternative way it to randomize their names when they are injected (so they can't be accessed directly by their class/variable names), and also make them impossible to enumerate by overwriting further browser APIs, which can be hidden using the previous 'rootkit-style' techniques.

\end{itemize}

Therefore, the current "temporary" step ahead over ad-blocker detectors of our prototype can be made into a \textbf{permanent} step ahead over ad-blocker detectors using those techniques.

\subsection{Visual tricks for ad-blocker users}

There are other techniques that can be used to display or hide part of the website content for ad-blocker users, albeit they are only visual tricks and not proper ad-blocker detector scripts.

Those techniques are not in the scope of this thesis and can be already dealt with with changes on filter lists (either by adding more filters, or using filter options such as \emph{generichide} or \emph{genericblock}~\cite{adblockplusgenerichideblock}).

\begin{figure}[H]
  \centering
  \caption{\textbf{Example visual trick for ad-blocker users. Since the CSS class 'adsbox' is included in the popular EasyList filter list, ad-blocker users see the first layer only, while non ad-blocker users see both layers, with the second layer overlaying the first.}}
    \begin{minted}[linenos, frame=lines]{html}
    <style>
    .layer
    {
        position:absolute;
        top:0; left:0;
        width:250px; height:100px;
        border: 5px solid red; background-color: white;
    }
    </style>
    <div class='layer'>You are using an ad-blocker.</div>
    <div class='layer adsbox'>You are not using an ad-blocker.</div>
    \end{minted}

    \begin{multicols}{2}

    \begin{figure}[H]
      \centering
      \caption{\textbf{Result without a filter list-based ad-blocker}}
      \includegraphics[width=\linewidth]{visualtricknotadblocker.png}
    \end{figure}

    \columnbreak

    \begin{figure}[H]
      \centering
      \caption{\textbf{Result with a filter list-based ad-blocker}}
      \includegraphics[width=\linewidth]{visualtrickadblocker.png}
     \end{figure}

    \end{multicols}
\end{figure}
