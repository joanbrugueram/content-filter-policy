\documentclass[a4paper,table]{article}
\usepackage{enumerate}
\usepackage{multicol}
\usepackage[english]{babel}
\usepackage[all]{xy}
\usepackage{color}
\usepackage{tikz}
\usepackage{textcomp}
\usetikzlibrary{shapes,arrows}
\usepackage{multirow}
\usepackage{indentfirst}
\usepackage[T1]{fontenc}
\usepackage[sc]{mathpazo}
\linespread{1.1} % Palatino needs more leading (space between lines)
\usepackage[utf8]{inputenc}
\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref}
\usepackage{graphicx, float}
\usepackage{tikz}
\usepackage{courier}
\usepackage{minted}
\usepackage[edges]{forest}
\usepackage[export]{adjustbox}
\usepackage{xspace}
\usepackage[a4paper, total={400pt, 598pt}]{geometry}

% https://tex.stackexchange.com/questions/60209/how-to-add-an-extra-level-of-sections-with-headings-below-subsubsection
\usepackage{titlesec}

\setcounter{secnumdepth}{4}

\titleformat{\paragraph}
{\normalfont\normalsize\bfseries}{\theparagraph}{1em}{}
\titlespacing*{\paragraph}
{0pt}{3.25ex plus 1ex minus .2ex}{1.5ex plus .2ex}

% https://tex.stackexchange.com/questions/588/how-can-i-change-the-margins-for-only-part-of-the-text
\def\changemargin#1#2{\list{}{\rightmargin#2\leftmargin#1}\item[]}
\let\endchangemargin=\endlist

% https://tex.stackexchange.com/questions/2441/how-to-add-a-forced-line-break-inside-a-table-cell
\newcommand{\specialcell}[2][c]{%
  \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}}

% https://tex.stackexchange.com/questions/237403/how-to-colour-branch-edge-on-a-tree-using-tikz
\tikzset{
  domnode/.style = {rounded corners,
  draw,
  align=center,
  top color=white,
  bottom color=blue!20, font=\footnotesize},
  domtextnode/.style = {bottom color=green!20, font=\footnotesize},
  domnodedisabled/.style = {bottom color=red!20, font=\footnotesize},
}

% Table cell colors
\newcommand{\tok}{\cellcolor{green!30}}
\newcommand{\twrn}{\cellcolor{yellow!30}}
\newcommand{\twrnhi}{\cellcolor{orange!40}}
\newcommand{\tko}{\cellcolor{red!25}}
\newcommand{\tyes}{\tok Yes}
\newcommand{\tno}{\tko No}

\begin{document}

%% TITLE PAGE
%% ----------

\begin{titlepage}
\begin{center}
\begin{figure}[htb]
\begin{center}
\includegraphics[width=6cm]{ub_color.pdf}
\end{center}
\end{figure}

\textbf{\LARGE Undergraduate thesis} \\
\vspace*{.5cm}
\textbf{\LARGE MATHEMATICS AND COMPUTER \\ SCIENCE DEGREE } \\
\vspace*{.5cm}
\textbf{\LARGE Facultat de Matemàtiques i Informàtica\\ Universitat de Barcelona} \\
\vspace*{1.5cm}
\rule{16cm}{0.1mm}\\
\begin{huge}
\textbf{Online advertisement blocker detection: A look at the state of the art for counter-detection and a proof-of-concept for new approaches} \\
\end{huge}
\rule{16cm}{0.1mm}\\

\vspace{1cm}

\begin{flushright}
\textbf{\LARGE Author: Joan Bruguera Micó}

\vspace*{2cm}

\renewcommand{\arraystretch}{1.5}
\begin{tabular}{ll}
\textbf{\Large Director:} & \textbf{\Large Lluís Garrido } \\
\textbf{\Large Submitted to:} & \textbf{\Large  Department de Matemàtica Aplicada i Anàlisi} \\
\\
\textbf{\Large Barcelona,} & \textbf{\Large June 2017 }
\end{tabular}

\end{flushright}

\end{center}

\end{titlepage}

\pagenumbering{roman} \setcounter{page}{0}

\section*{Abstract (English)}

In the last years, there has been a surge in the usage of ad-blocking software among Internet users, mainly motivated by intrusive advertisements and rising awareness of the privacy and security implications of advertisements. As a response, many websites have started implementing ad-blocker detection scripts, which detect whether or not an user has an ad-blocking software.

In this work, we study the implications and technical underpinnings of those scripts and document relatively simple workarounds ad-blocking software could implement to avoid detection by those scripts. We also present a prototype implementing the explained techniques over a set of test cases.

\section*{Abstract (Català)}

En els últims anys, hi ha hagut una explosió en l'ús de software de bloqueig de publicitat per part dels usuaris d'Internet, principalment motivada per la publicitat intrusiva i una creixent consciència de les implicacions de privacitat i seguretat dels anuncis. Com a resposta, moltes pàgines web han començat a implementar scripts detectors de bloquejadors de publicitat, els quals detecten si l'usuari té o no un software de bloqueig de publicitat.

En aquest treball, estudiem les implicacions i principis tècnics d'aquests scripts i documentem mètodes relativament senzills que es podrien implementar en el software de bloqueig de publicitat per tal d'evitar la detecció per aquest tipus d'scripts. També presentem un prototip que implementa les tècniques explicades sobre un conjunt de casos de prova.

\newpage

\tableofcontents

%% CONTENT
%% -------

\newpage
\pagenumbering{arabic}
\include{thesis_introduction}

\include{thesis_internetadvertising}

\include{thesis_principlesadblockingsoftware}

\include{thesis_technicaldetails}

\include{thesis_results}

\include{thesis_limitations}

\include{thesis_conclusions}

\include{thesis_appendices}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newpage

\begin{thebibliography}{999}

\bibitem{firefoxaddonsmostpopular} Add-ons for Mozilla Firefox, \emph{Most Popular Extensions [for Mozilla Firefox]}, \url{https://addons.mozilla.org/en-US/firefox/extensions/?sort=users}
\bibitem{adfender} AdFender, \emph{AdFender - AdFender gives you a faster, less cluttered and more private web surfing experience!}, \url{https://www.adfender.com}
\bibitem{adguard} AdGuard, \emph{AdGuard - The world's most advanced ad blocker!}, \url{https://adguard.com}
\bibitem{braveadreplacement} Brave browser (Brave Software), \emph{What is Ad Replacement?}, \url{https://www.brave.com/about_ad_replacement.html}
\bibitem{bravebrowser} Brave Software, \emph{Brave Software - Building a Better Web}, \url{https://www.brave.com/}
\bibitem{abpfilterparser} Brian R. Bondy et al, \emph{abp-filter-parser (Github)}, \url{https://github.com/bbondy/abp-filter-parser}
\bibitem{browserify} Browserling, \emph{Browserify}, \url{http://browserify.org/}
\bibitem{chromewebstore} Chrome Web Store, \emph{Chrome Web Store}, \url{https://chrome.google.com/webstore}
\bibitem{supercookiestrackyou} CNN, \emph{"Super cookies" track you, even in privacy mode}, \url{http://money.cnn.com/2015/01/09/technology/security/super-cookies/index.html}
\bibitem{privacybadgerdeteriorate} Daniel Aleksandersen, \emph{EFF’s Privacy Badger will deteriorate your browser experience}, \url{https://ctrl.blog/entry/privacy-badgering}
\bibitem{easylist} EasyList Filter List Project, \emph{EasyList - Overview}, \url{https://easylist.to/}
\bibitem{privacybadger} Electronic Frontier Foundation, \emph{Electronic Frontier Foundation - Privacy Badger}, \url{https://www.eff.org/privacybadger}
\bibitem{panopticlickbrowserfingerprinting} Electronic Frontier Foundation, \emph{Panopticlick - What is browser fingerprinting?}, \url{https://panopticlick.eff.org/about#browser-fingerprinting}
\bibitem{stacktracejs} Eric Wendelin, Victor Homyakov, Oliver Salzburg et al., \emph{JavaScript framework-agnostic, micro-library for getting stack traces in all web browsers}, \url{https://www.stacktracejs.com/}
\bibitem{adblockaceptableads} Eyeo GmbH, {\emph Allowing acceptable ads in Adblock Plus}, \url{https://adblockplus.org/acceptable-ads}
\bibitem{adblockplus} Eyeo GmbH, \emph{Adblock Plus - Surf the web without annoying ads}, \url{https://adblockplus.org/}
\bibitem{adblockplusfilters} Eyeo GmbH, \emph{Adblock Plus filters}, \url{https://adblockplus.org/en/filters}
\bibitem{adblockplususersurvey} Eyeo GmbH, \emph{Adblock Plus user survey results}, \url{https://adblockplus.org/blog/adblock-plus-user-survey-results-part-1}, \url{https://adblockplus.org/blog/adblock-plus-user-survey-results-part-2}, \url{https://adblockplus.org/blog/adblock-plus-user-survey-results-part-3}
\bibitem{adblockplusgenerichideblock} Eyeo GmbH, \emph{New filter options \$generichide and \$genericblock}, \url{https://adblockplus.org/development-builds/new-filter-options-generichide-and-genericblock}
\bibitem{therearenoacceptableads} FiveFilters, \emph{There are no acceptable ads}, \url{https://github.com/fivefilters/block-ads/wiki/There-are-no-acceptable-ads}
\bibitem{forbesadblockingplanresults} Forbes, \emph{Inside Forbes: More Numbers On Our Ad Blocking Plan -- and What's Coming Next}, \url{https://www.forbes.com/sites/lewisdvorkin/2016/02/10/inside-forbes-more-numbers-on-our-ad-blocking-plan-and-whats-coming-next/}
\bibitem{chromepopupblockers} Google, \emph{Block or allow pop-ups in Chrome}, \url{https://support.google.com/chrome/answer/95472?co=GENIE.Platform%3DDesktop&hl=en}
\bibitem{googlecontributor} Google, \emph{Google Contributor - Introduction}, \url{https://contributor.google.com}
\bibitem{ublockorigin} gorhill (Raymond Hill), \emph{uBlock Origin - An efficient blocker add-on for various browsers. Fast, potent, and lean.}, \url{https://github.com/gorhill/uBlock}
\bibitem{ublockoriginperformancefirstparty} gorhill (Raymond Hill), \emph{uBlock vs. ABP: efficiency compared}, \url{https://github.com/gorhill/uBlock/wiki/uBlock-vs.-ABP:-efficiency-compared}
\bibitem{umatrixrepo} gorhill (Raymond Hill), \emph{uMatrix: Point and click matrix to filter net requests according to source, destination and type}, \url{https://github.com/gorhill/uMatrix}
\bibitem{perceptualadhighlighter} Grant Storey, Dillon Reisman, Jonathan Mayer, Arvind Narayanan, \emph{Perceptual Ad Highlighter}, \url{https://chrome.google.com/webstore/detail/perceptual-ad-highlighter/mahgiflleahghaapkboihnbhdplhnchp}
\bibitem{visualadblockerpaper} Grant Storey, Dillon Reisman, Jonathan Mayer, Arvind Narayanan, \emph{The Future of Ad Blocking: An Analytical Framework and New Techniques}, \url{http://randomwalker.info/publications/ad-blocking-framework-techniques.pdf}
\bibitem{noscript} InformAction, \emph{NoScript - JavaScript/Java/Flash blocker for a safer Firefox experience!}, \url{https://noscript.net/}
\bibitem{browsermicrotasks} Jake Archibald, \emph{Tasks, microtasks, queues and schedules}, \url{https://jakearchibald.com/2015/tasks-microtasks-queues-and-schedules/}
\bibitem{visualadblocker} Jason Koebler (Motherboard, VICE), \emph{Princeton’s Ad-Blocking Superweapon May Put an End to the Ad-Blocking Arms Race}, \url{https://motherboard.vice.com/en_us/article/princetons-ad-blocking-superweapon-may-put-an-end-to-the-ad-blocking-arms-race}
\bibitem{httpmitmproxy} Joe Ferner et al, \emph{http-mitm-proxy (Github)}, \url{https://github.com/joeferner/node-http-mitm-proxy}
\bibitem{adblockprotector} jspenguin2017, \emph{AdBlock Protector}, \url{https://github.com/jspenguin2017/AdBlockProtector}
\bibitem{internetexplorerdonottrack} Microsoft, \emph{Use Do Not Track in Internet Explorer 11}, \url{https://support.microsoft.com/en-us/help/17288/windows-internet-explorer-11-use-do-not-track}
\bibitem{mozilladevelopernetwork} Mozilla Developer Network, \emph{Web technology for developers | MDN}, \url{https://developer.mozilla.org/en-US/docs/Web}
\bibitem{firefoxclicktoplay} Mozilla Support, \emph{Set Adobe Flash to "click to play" on Firefox}, \url{https://support.mozilla.org/en-US/kb/set-adobe-flash-click-play-firefox}
\bibitem{firefoxtrackingprotection} Mozilla, \emph{Tracking Protection in Private Browsing}, \url{https://support.mozilla.org/en-US/kb/tracking-protection-pbm}
\bibitem{afirstlookatadblockdetection} Muhammad Haris Mughees, Zhiyun Qian, Zubair Shafiq, Karishma Dash, Pan Hui, \emph{A First Look at Ad-block Detection – A New Arms Race on the Web}, \url{https://arxiv.org/pdf/1605.05841.pdf}
\bibitem{nodejs} Node.js Foundation, \emph{Node.js}, \url{https://nodejs.org}
\bibitem{nodejsnpm} npm, Inc., \emph{npm}, \url{https://www.npmjs.com/}
\bibitem{thecostofadblocking} PageFair \& Adobe, \emph{The cost of ad blocking}, \url{https://downloads.pagefair.com/wp-content/uploads/2016/05/2015_report-the_cost_of_ad_blocking.pdf}
\bibitem{pagefair2017adblockreport} PageFair, \emph{2017 Adblock Report}, \url{https://pagefair.com/blog/2017/adblockreport/}
\bibitem{browserrelayout} Paul Irish (et al), \emph{What forces layout/reflow. The comprehensive list.} (and related links), \url{https://gist.github.com/paulirish/5d52fb081b3570c81e3a} (and related links)
\bibitem{pihole} Pi-Hole, \emph{Pi-hole\texttrademark: A black hole for Internet advertisements}, \url{https://pi-hole.net/}
\bibitem{theeuisfinewithblockingadblockers} Quartz Media, \emph{The EU is totally fine with with publishers blocking ad blockers}, \url{https://qz.com/882462/the-eu-is-totally-fine-with-with-publishers-blocking-ad-blockers/}
\bibitem{ublockoriginperformancethirdparty} Raymond.CC, \emph{10 Ad Blocking Extensions Tested for Best Performance}, \url{https://www.raymond.cc/blog/10-ad-blocking-extensions-tested-for-best-performance/view-all/}
\bibitem{antiadblockkiller} Reek, \emph{Anti-Adblock Killer helps you keep your Ad-Blocker active, when you visit a website and it asks you to disable. (github)}, \url{https://github.com/reek/anti-adblock-killer}
\bibitem{blockadblock} sitexz, \emph{BlockAdBlock - Allows you to detect the extension AdBlock (and other)}, \url{https://github.com/sitexw/BlockAdBlock}
\bibitem{acorn} Tern JavaScript tooling software, \emph{Acorn - A tiny, fast JavaScript parser, written completely in JavaScript (Github)}, \url{https://github.com/ternjs/acorn}
\bibitem{chromiumfingerprinting} The Chromium Projects (Google), \emph{Chromium Security - Technical analysis of client identification mechanisms - Browser-level fingerprints}, \url{https://www.chromium.org/Home/chromium-security/client-identification-mechanisms#TOC-Browser-level-fingerprints}
\bibitem{acceptableadsmoney} The Guardian, \emph{Adblock Plus launching platform to sell 'acceptable' ads}, \url{https://www.theguardian.com/business/2016/sep/13/adblock-plus-launching-platform-to-sell-acceptable-ads}
\bibitem{advertisementsdatausage} The Guardian, \emph{Adblocking: advertising 'accounts for half of data used to read articles'}, \url{https://www.theguardian.com/media/2016/mar/16/ad-blocking-advertising-half-of-data-used-articles}
\bibitem{googlecontributorarticle} The Guardian, \emph{Google Contributor: can I really pay to remove ads?}, \url{https://www.theguardian.com/technology/2014/nov/21/google-contributor-pay-remove-ads}
\bibitem{antiadblockerslegalchallenges} The Register, \emph{Ad-blocker blocking websites face legal peril at hands of privacy bods}, \url{https://www.theregister.co.uk/2016/04/23/anti_ad_blockers_face_legal_challenges/}
\bibitem{sitesthatblockadblockerssuffer} The Stack, \emph{Sites that block adblockers seem to be suffering}, \url{https://thestack.com/world/2016/04/21/sites-that-block-adblockers-seem-to-be-suffering/}
\bibitem{ublockoriginnoacceptableads} uBlock Origin, \emph{Add acceptable ads (Github issue)}, \url{https://github.com/chrisaljoudi/uBlock/issues/66}
\bibitem{visibilityhiddenvsdisplaynone} W3Schools, \emph{CSS Layout - The display Property - Hide an Element - display:none or visibility:hidden?}, \url{https://www.w3schools.com/css/css_display_visibility.asp}
\bibitem{webopedia} Webopedia, \emph{Webopedia: Online Tech Dictionary for IT Professionals}, \url{http://www.webopedia.com} (you can look for the referred term here)
\bibitem{bravepublishersstrikeback} WIRED, \emph{Publishers Strike Back at a Browser That Replaces Their Ads}, \url{https://www.wired.com/2016/04/brave-software-publishers-respond/}
\bibitem{malvertising} WIRED, \emph{Why Malvertising Is Cybercriminals’ Latest Sweet Spot}, \url{https://www.wired.com/insights/2014/11/malvertising-is-cybercriminals-latest-sweet-spot/}

\end{thebibliography}

Icons from the Autü Plasma 5 theme available at \url{https://github.com/lucasnota/Antu-U}. \\

All web sources checked on June 2017. \\

You may find amended versions at \url{https://bitbucket.org/joanbrugueram/content-filter-policy}.

\end{document}
