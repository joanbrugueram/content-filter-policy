\section{Appendices}

\subsection{Testing the prototype}

Here we expose some brief instructions to get the prototype ad-blocker we have developed implementing the proposed techniques running.

\subsubsection{Installing \emph{Node.js}}

You will need to install \emph{Node.js}~\cite{nodejs} in order to run the prototype. We tested it under various recent versions, though we recommend using \emph{v6.11.0 LTS}.

\subsubsection{Obtaining the prototype}

You should have received a copy of the prototype along with this document. Otherwise, you can download it from \url{https://bitbucket.org/joanbrugueram/content-filter-policy} using \emph{Git}.

Inside the copy of the prototype, there should be three folders:

\begin{itemize}

\item \textbf{proxy:} The implementation of the proxy server. This is the core of the ad-blocker prototype.

\item \textbf{test\_cases:} This is a set of tests that can be used to test the effectiveness of the prototype in a consistent way.

\item \textbf{thesis:} This is the \LaTeX\xspace source of this document.

\end{itemize}

\subsubsection{Installing the dependencies and starting the proxy server}

In order to install all the required dependencies and start the proxy server, you should open a terminal window and type \texttt{npm install \&\& npm run start}. This will start the proxy server at your current local IP address on port 8081. You should keep this terminal window open during the next steps.

\begin{figure}[H]
  \centering
  \caption{\textbf{Setting up the proxy server}}
  \includegraphics[width=0.7\linewidth]{startproxy.png}
\end{figure}

\subsubsection{Starting the test case server}

In order to install all the required dependencies and start the test case server, you should open a terminal window and type \texttt{npm install \&\& npm run start}. This will start the test case server at your current local IP address on port 8080. You should keep this terminal window open during the next steps.

\begin{figure}[H]
  \centering
  \caption{\textbf{Setting up the test case server}}
  \includegraphics[width=0.7\linewidth]{starttestcases.png}
\end{figure}

\subsubsection{Browse the test cases with a web browser}

Start by completely clearing your web browser's cache and disabling any currently installed ad-blockers, since those may infer with the operation of the prototype.

You should then configure your web browser or operating system to connect through the proxy server at your IP address and port 8081. This will depend on the web browser or operating system.

\begin{figure}[H]
  \centering
  \caption{\textbf{Example proxy configuration with Firefox}}
  \includegraphics[width=0.7\linewidth]{configureproxy.png}
\end{figure}

After this, opening the URL at your IP address and port 8080 (\url{http://YOUR.IP.ADDRESS.HERE:8080}) will show you the list of the test cases.

\begin{figure}[H]
  \centering
  \caption{\textbf{Browsing the test cases with Firefox}}
  \includegraphics[width=0.7\linewidth]{browsetestcases.png}
\end{figure}


\subsubsection{Further steps}

\begin{itemize}

\item \textbf{Editing the configuration file:} You can fine-tune various options (filter lists, options for the techniques, etc.) by copying the sample file at \texttt{proxy/proxyConfig.json.defaults} to \texttt{proxyConfig.json} and editing it accordingly. The configuration file contains details about the meaning of each available configuration.

\item \textbf{Browsing secure (HTTPS) websites with the prototype:} Due to browser security measures, browsers will not allow navigating to secure (HTTPS) websites under a man-in-the-middle proxy. If you wish to authorize your browser to browse HTTPS websites under the prototype ad-blocker, you should import the certification authority defined under the (hidden) \texttt{proxy/.http-mitm-proxy/certs/ca.pem} file to your web browser.

\end{itemize}

\subsubsection{Test cases}
\label{subsubsec:testcases}

The folder \texttt{test\_cases} contains a set of tests designed to exercise ad-blockers in order to test if they are effective, and their ability to be detected. \\

Those test cases have been developed based on a using both ready-made ad-blocker detector solutions (such as \emph{BlockAdBlock}~\cite{blockadblock}), practical examples analyzed by research~\cite{afirstlookatadblockdetection} and our own knowledge of ad-blocker detector techniques.

The test cases are typically brief and self-contained, so they can be easily understood by looking in the source code. \\

On the folder, the test cases are classified under different prefixes, namely:

\begin{itemize}

\item \textbf{content\_*:} Tests related to ad-blocker detection through \textbf{element hiding rules}.

\item \textbf{request\_*:} Tests related to ad-blocker detection through \textbf{address blocking rules}.

\item \textbf{correctness\_*:} Tests of the implementation of the prototype in various "tricky" cases.

\item \textbf{privacy\_*:} Tests of the implementation in the prototype of privacy-preserving techniques.

\item \textbf{performance\_*:} Performance tests of the implementation in the prototype.

\item \textbf{outofproject\_*:} Tests of the limitations of the prototype.

\end{itemize}

The expected results of the test cases without an ad-blocker, with a regular blacklist-based ad-blocker (such as \emph{uBlock Origin}), and under the prototype, are:

\begin{figure}[H]
  \centering
  \caption{\textbf{Expected results of the test cases without an ad-blocker, with a regular blacklist-based ad-blocker, and with our prototype}}
  {\footnotesize
  \begin{changemargin}{-0.5cm}{-0.5cm}
  \begin{center}
      \begin{tabular}{| l | l | l | l |}
      \hline
      \textbf{Expected result} & \textbf{Without an ad-blocker} & \textbf{With an ad-blocker} & \textbf{With the prototype} \\ \hline
      \textbf{content\_*:} & \twrnhi Ads + Ad-blocker not detected & \twrnhi No ads + Ad-blocker detected & \tok No ads + Ad-blocker not detected \\ \hline
      \textbf{request\_*:} & \twrnhi Ads + Ad-blocker not detected & \twrnhi No ads + Ad-blocker detected & \tok No ads + Ad-blocker not detected \\ \hline
      \textbf{correctness\_*:} & \tok OK & \tok OK & \tok OK \\ \hline
      \textbf{privacy\_*:} & \twrn Some OK, some fail & \twrn Some OK, some fail & \tok OK \\ \hline
      \textbf{performance\_*:} & \tok OK & \tok OK & \twrn Some OK, some fail \\ \hline
      \textbf{outofproject\_*:} & \tok OK & \twrn Some OK, some fail & \twrn Some OK, some fail \\ \hline
      \end{tabular}
  \end{center}
  \end{changemargin}
  }
\end{figure}

\subsection{Browser fingerprinting by enabled filter lists}
\label{subsec:browserfingerprintingbyfilterlists}

One can use the same techniques used to detect blacklist-based ad-blockers by websites to create a fingerprint based on which filter lists the user has enabled.

The technique used to create the fingerprint is simple in the face of how ad-blocker detector lists work. Since there are relatively few widely used filter lists, which usually contain hundreds of thousands of filters, there is, with a very high degree of certainty, a set of filters that is unique to that filter list. By detecting whether or not those filters are being applied by a web browser (a specific DOM element is hidden or an address is blocked), they can be used to detect whether this filter list is enabled or not. \\

This fingerprinting technique can be used without requiring the user to have JavaScript enabled, since ad-blocker detection by address blocking rules can be done by the website server without relying on client-side scripts. This is remarkable, since JavaScript is the main vector used for fingerprinting, and is typically disabled by those trying to avoid being fingerprinted, such as users of \emph{Tor} or \emph{i2p}.

Additionally, on most cases, we expect that this fingerprint not to be enough to track individual users on his own, due to the high prevalence of users with the same ad-blocker settings.  \\

An illustration of fingerprinting by filter lists can be seen in the \path{privacy_fingerprint_ad_lists} test case.
