# ContentFilterPolicy technical details

## Setup

### Loading AdBlock filter lists

Adblock filter lists are parsed using an open source library called [abp-filter-parser](https://github.com/bbondy/abp-filter-parser). Those lists will provide us a list of the content that should be filtered on the websites the user visits.

### Setting up a proxy server

A MITM proxy server is set up using an open source library called [http-mitm-proxy](https://github.com/joeferner/node-http-mitm-proxy). This will allow us to intercept and rewrite requests in order to filter content.

## Interception and transformation

During normal operation of the proxy server, when a request is received, it will be forwarded to the original server, and once it's received, it will be transformed in order to inject the necessary components for content filtering.

* **Content filtering stylesheet:** For each HTML document that is requested through the proxy server, a CSS style sheet is generated and injected that will allow the detection of filtered content defined by CSS selectors on the AdBlock filter list.

  This is accomplished by adding a custom CSS variable `--filtered` to each selector specified by the stylesheet, e.g.:

  ```css
  .ad { --filtered: true; }
  ```

  This does nothing by itself, but will allow us to easily check if an element is filtered or not later on.

* **Script sandboxing**: When a JavaScript script that is targetted by the filter lists is requested, additional code will be added to the script that will allow certain operations to be blocked or altered by filter policies, and detecting whether an element has been created by a filtered script or not.

  This is done by setting a 'sandbox' flag when a code path passes through a filtered script and removing it once the code path finishes.

  ```javascript
  window.__enterSandboxContext__(); // Sets the sandbox flag
  // Original script or original function goes here
  window.__leaveSandboxContext__(); // Unsets the sandbox flag
  ```

  This flag can then be detected by the filtering engine and from the filter policies configured by the user.

* **Content filtering engine:** A set of scripts is also injected to each HTML document that is requested through the proxy server. Those scripts contain the engine that allows the content filtering to work.

  The way this currently works is through the use of [Mutation Observers](https://developer.mozilla.org/es/docs/Web/API/MutationObserver) and [Mutation Events](https://developer.mozilla.org/en-US/docs/Web/Guide/Events/Mutation_events) used to detect when an element from the DOM is added, removed, or modified.

  The content filtering engine checks each mutation over DOM elements and checks (thanks to the content filtering stylesheet and the additional code added to the filtered scripts) whether it should be filtered or not. If it should be filtered, it then applies the filter policies configured by the user over those elements.

  Basically, in pseudocode:

  ```plain
  When an element is added, removed or modified
    If the element has been targetted by a CSS filter or was created by a filtered script
      Mark the element as filtered
      Apply filter policies over the element
  ```

* **Content filter policies for content**: An additional set of scripts is also injected to each HTML document that is requested through the proxy server. Those scripts contain the filter policies defined by the user to be applied over each filtered element, such as, for example:

  * Hide filtered elements

  * Add a border around filtered elements

  * Etc.

* **Content filter policies for sandboxing**: Thanks to Javascript's support for [aspect-oriented programming](https://en.wikipedia.org/wiki/Aspect-oriented_programming) (thanks to prototype-based inheritance), certain operations (e.g. AJAX, cookies), can be blocked or altered by the filter policies, effectively 'sandboxing' the script and avoiding certain privacy and data usage risks, such as, for example:

  * AJAX requests from sandboxed scripts

  * Reading cookies from sandboxed scripts

  * Etc.
