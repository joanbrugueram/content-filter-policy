'use strict';

const fs = require('fs');
const stripJsonComments = require('strip-json-comments');

const proxyConfigDefaultFile = 'proxyConfig.json.defaults';
const proxyConfigFile = 'proxyConfig.json';

/**
 * Read a configuration file.
 * @param  {String}   filePath File name of the configuration file.
 * @param  {Function} cb  Callback which is called after finishing,
 *                        with an optional error parameter,
 *                        and a structure containing the configuration.
 */
function readConfigFile(filePath, cb) {
    fs.readFile(filePath, (err, configFileJson) => {
        if (err) {
            cb(err, null);
            return;
        }

        const configFile = JSON.parse(stripJsonComments(
            configFileJson.toString()));
        cb(null, configFile);
    });
}
/**
 * Create a new configuration manager.
 */
function ConfigManager() {
}

/**
 * Get the current configuration of the application.
 * @param  {Function} cb  Callback which is called after finishing,
 *                        with an optional error parameter,
 *                        and a structure containing the configuration.
 */
ConfigManager.prototype.get = function(cb) {
    readConfigFile(proxyConfigDefaultFile, (err, defaultConfig) => {
        if (err) {
            console.warn('WARNING: Could not read ' +
                proxyConfigDefaultFile + '.');
            cb(err, null);
            return;
        }

        readConfigFile(proxyConfigFile, (err, config) => {
            if (err) {
                console.warn('WARNING: Could not read ' + proxyConfigFile +
                    ', using default configuration.');
                cb(null, defaultConfig);
                return;
            }

            const mergedConfig = Object.assign({}, defaultConfig, config);
            cb(null, mergedConfig);
        });
    });
};

module.exports = ConfigManager;
