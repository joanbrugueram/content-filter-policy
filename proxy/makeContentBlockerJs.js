'use strict';

const fs = require('fs');
const browserify = require('browserify');
const streamToString = require('stream-to-string');
const async = require('async');

/**
 * Generate the Javascript code used for content filtering.
 * @param  {Structure} config           Content filter configuration.
 * @param  {Function} callback          Callback for generated code.
 */
function generateCode(config, callback) {
    async.parallel({
        bootstrapCode: function(asyncCallback) {
            const b = browserify();
            b.add('./clientSideFilterEngineBootstrap.js');
            const stream = b.bundle();

            streamToString(stream, asyncCallback);
        },
        contentFilterCode: function(asyncCallback) {
            const b = browserify();
            b.add('./clientSideFilterEngine.js');
            const stream = b.bundle();

            streamToString(stream, function(err, scriptText) {
                if (err) {
                    asyncCallback(err);
                    return;
                }

                let policiesText = '';
                config.policies.forEach(function(policy) {
                    policiesText += fs.readFileSync(policy.file).toString()
                        .replace(/'@@INJECT_POLICY_PARAMETERS_HERE@@'/,
                        JSON.stringify(policy.params));
                });

                const scriptTextWithPolicies = scriptText.replace(
                    /'@@INJECT_POLICIES_HERE@@'/g,
                    policiesText).replace(
                    /'@@INJECT_CONFIG_HERE@@'/g,
                    JSON.stringify(config));
                asyncCallback(null, scriptTextWithPolicies);
            });
        },
    }, callback);
};

module.exports = {generateCode: generateCode};
