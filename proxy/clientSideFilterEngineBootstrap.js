(function() {
    const serverDefs = require('./serverDefs');

    /**
     * Checks if the current window context
     * has access to the given window context.
     * (E.g. Cross domain IFrame windows often can't access the parent window).
     * @param  {Object}  wnd Window to check.
     * @return {Boolean}     true if it has access to the window context,
     *                       false otherwise.
     */
    function hasAccessToWindow(wnd) {
        try {
            wnd.document; // Should not be easy to override, if possible...
            return true;
        } catch( e ) {
            return false;
        }
    }

    /**
     * Gets the topmost window context to which this window has access.
     * @return {Object} The topmost accessible window context.
     */
    function getTopmostAccessibleWindow() {
        let current = window;
        while (current !== current.parent && hasAccessToWindow(current.parent))
            current = current.parent;
        return current;
    }

    const accessibleWindowTop = getTopmostAccessibleWindow();

    if (window === accessibleWindowTop &&
        window.__CFP__ === undefined) {
        // Start ContentFilterPolicy and attach on top window
        document.write('<script type="text/javascript" ' +
            'src="' + serverDefs.injectJavascriptName + '"><' + '/script>');
    } else if (window === accessibleWindowTop &&
        window.__CFP__ === undefined) {
        console.log('WARNING: CFP bootstrap script reinjected on top window.');
    } else if (window !== accessibleWindowTop &&
        accessibleWindowTop.__CFP__ === undefined) {
        console.log('WARNING: CFP not defined on non-parent window!');
    } else if (window !== accessibleWindowTop &&
        // Attach ContentFilterPolicy on child window
        accessibleWindowTop.__CFP__ !== undefined) {
        window.__CFPSCRIPTCTL__ = accessibleWindowTop.__CFPSCRIPTCTL__;
        window.__CFPSANDBOXCTL__ = accessibleWindowTop.__CFPSANDBOXCTL__;
        new accessibleWindowTop.__CFPCLASS__(window,
            accessibleWindowTop.__CFP__,
            accessibleWindowTop.__CFPSCRIPTCTL__,
            accessibleWindowTop.__CFPSANDBOXCTL__);
    }
})();
