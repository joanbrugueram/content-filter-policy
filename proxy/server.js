'use strict';

const Proxy = require('http-mitm-proxy');
const ABPFilterParser = require('abp-filter-parser');
const htmlTransformer = require('./htmlTransformer');
const javascriptTransformer = require('./javascriptTransformer');
const makeContentBlockerCss = require('./makeContentBlockerCss');
const makeContentBlockerJs = require('./makeContentBlockerJs');
const FilterListManager = require('./filterListManager');
const ConfigManager = require('./configManager');
const ip = require('ip');
const url = require('url');
const querystring = require('querystring');
const serverDefs = require('./serverDefs');
const contentTypeParser = require('content-type-parser');

const ipAddress = ip.address();

let requestIdCounter = 1;

/**
 * Strips the port number out of a host string (e.g. from a server request).
 * Example: localhost:1234 --> localhost
 * @param  {string} host Host string, optionally containing a port number.
 * @return {string}      Host string without a port number.
 */
function stripPortFromHost(host) {
    const n = host.indexOf(':');
    return host.substring(0, n != -1 ? n : host.length);
}

/**
 * Create a new transformer that just passes data through without changing.
 */
function PassthroughTransformer() {
    this.onChunk = function(chunk) {
        return chunk;
    };

    this.onFinish = function() {
        return new Buffer(0);
    };
}

/**
 * Create a new transformer for Javascript scripts.
 * @param  {Boolean} options    Options for script transformation:
 * - enterContext    Enter script context on every function.
 * - sandbox         Add code to sandbox script actions.
 * - wrapCode        Wrap code in function before transforming.
 *                   (Used to transform error handlers)
 */
function JavascriptTransformer(options) {
    const chunks = [];

    this.onChunk = function(chunk) {
        chunks.push(chunk);
        return null;
    };

    this.onFinish = function() {
        const body = Buffer.concat(chunks);
        return Buffer.from(javascriptTransformer.transformJavascriptString(
            body.toString(), options), 'utf8');
    };
}

/**
 * Get transformer instance for the given content-type.
 * @param  {String} rawContentType Content of the content-type header.
 * @param {Boolean} bootstrapCode Content filter Javascript bootstrap code.
 * @param {Boolean} jsTransformOpts Options for the Javascript transformer.
 * @return {Object}                Instance of the content transformer.
 */
function getContentTransformer(rawContentType, bootstrapCode, jsTransformOpts) {
    const parsedCt = rawContentType ? contentTypeParser(rawContentType) : null;
    const mime = parsedCt ? (parsedCt.type + '/' + parsedCt.subtype) : null;

    if (mime && htmlTransformer.isHtmlMimeType(mime)) {
        return new htmlTransformer.HtmlTransformer(bootstrapCode);
    } else if (mime && javascriptTransformer.isJavascriptMimeType(mime)
        && !javascriptTransformer.isNullTransform(jsTransformOpts)) {
        return new JavascriptTransformer(jsTransformOpts);
    } else {
        return new PassthroughTransformer();
    }
}

/**
 * Gets the host that caused the request to the server (referrer or same host).
 * @param  {incomingMessage} incomingMessage Incoming message structure.
 * @return {String}                 Original requesting host.
 */
function getRequestingHost(incomingMessage) {
    if (incomingMessage.headers.referer) {
        const parsedUrl = url.parse(incomingMessage.headers.referer);
        return parsedUrl.host;
    }

    return incomingMessage.headers.host;
}

/**
 * Handle a request to the proxy server.
 * @param  {Structure}   ctx               Request context.
 * @param  {Function} callback          Callback to be called once the request
 *                                      is handled.
 * @param  {Structure} config           Proxy configuration.
 * @param  {Array}     filterList       Filter lists to apply.
 * @param  {String}    filterEngineJs   Code to inject for the filter engine.
 */
function handleProxyRequest(ctx, callback, config, filterList, filterEngineJs) {
    const requestId = requestIdCounter++;

    /**
     * Log information corresponding to the current request.
     * @param  {String} str Information to log.
     */
    function log(str) {
        console.log('[ID=' + requestId + '] ' + str);
    }

    // Enable GZIP middleware for uncompressing it
    ctx.use(Proxy.gunzip);
    // Force the encoding to be GZIP (or uncompressed), disable schp, brotli, ..
    // Should not be required as it's included in this commit, but doesn't work
    // https://github.com/joeferner/node-http-mitm-proxy/pull/84
    ctx.proxyToServerRequestOptions.headers['accept-encoding'] = 'gzip';

    const host = ctx.clientToProxyRequest.headers.host;
    const url = ctx.clientToProxyRequest.url;

    log('REQUEST TO HOST ' + host + ', URL: ' + url);

    // Only proxy local requests, at the moment
    if (config.onlyInterceptLocal) {
        const realHost = stripPortFromHost(host);
        if (realHost !== ipAddress) {
            log('>> PASS-THROUGH: Not a local address.');
            callback();
            return;
        }
    }

    const requestingHost = getRequestingHost(ctx.clientToProxyRequest);

    // Handle 'special' internal URLs
    if (url.indexOf(serverDefs.injectStyleSheetName) > -1) {
        log('>> INJECTING CONTENT FILTER CSS');
        const filtersCss = makeContentBlockerCss.generateStylesheetFromFilters(
            filterList, requestingHost);
        ctx.proxyToClientResponse.writeHead(200, {
            'Content-Type': 'text/css',
            'Access-Control-Allow-Origin': '*',
        });
        ctx.proxyToClientResponse.end(filtersCss);
        return;
    } else if (url.indexOf(serverDefs.injectJavascriptName) > -1) {
        log('>> INJECTING CONTENT FILTER JS');
        ctx.proxyToClientResponse.writeHead(200, {
            'Content-Type': 'text/javascript',
            'Access-Control-Allow-Origin': '*',
        });
        ctx.proxyToClientResponse.end(filterEngineJs.contentFilterCode);
        return;
    } else if (url.indexOf(serverDefs.generateStatusCodePath + '?') > -1) {
        const queryString = url.substring(
            url.indexOf(serverDefs.generateStatusCodePath)
            + serverDefs.generateStatusCodePath.length + '?'.length);
        const queryParams = querystring.parse(queryString);
        log('>> GENERATING STATUS CODE = ' + queryParams.code);
        ctx.proxyToClientResponse.writeHead(queryParams.code || 200, {
            'Access-Control-Allow-Origin': '*',
        });
        ctx.proxyToClientResponse.end('');
        return;
    } else if (url.indexOf(serverDefs.checkRequestPath + '?') > -1) {
        const queryString = url.substring(
            url.indexOf(serverDefs.checkRequestPath)
            + serverDefs.checkRequestPath.length + '?'.length);
        const queryParams = querystring.parse(queryString);
        const checkResult = ABPFilterParser.matches(filterList,
            queryParams.url, {domain: requestingHost});
        log('CHECKING URL: ' + queryParams.url + ' --> ' + checkResult);
        ctx.proxyToClientResponse.writeHead(200, {
            'Access-Control-Allow-Origin': '*',
        });
        ctx.proxyToClientResponse.end(checkResult.toString());
        return;
    }

    const isBannedUrl = ABPFilterParser.matches(filterList,
        url, {domain: requestingHost});

    if (isBannedUrl) {
        log('>> AD RESOURCE DETECTED');
		if (config.requestPolicy === serverDefs.requestPolicy.notFound) {
            log('>> SIMULATING 404 NOT FOUND FOR BLOCKED RESOURCE');
            ctx.proxyToClientResponse.writeHead(404);
            ctx.proxyToClientResponse.end('');
            return;
        } else if (config.requestPolicy === serverDefs.requestPolicy.blank) {
            log('>> SIMULATING 200 OK (BLANK) FOR BLOCKED RESOURCE');
            ctx.proxyToClientResponse.end('');
            return;
        }
    }

    let transformer;
    ctx.onResponse(function(ctx, callback) {
        transformer = getContentTransformer(
            ctx.serverToProxyResponse.headers['content-type'],
            filterEngineJs.bootstrapCode, {
                enterContext: config.changeContext ===
                    serverDefs.changeContext.onEnterScript,
                sandbox: isBannedUrl && config.detectSandbox ===
                    serverDefs.detectSandbox.onEnterScript &&
                    config.requestPolicy ===
                    serverDefs.requestPolicy.sandbox,
            });
        log('>> DETECTED TRANSFORMER FOR INTERCEPTION: ' +
            transformer.constructor.name);
        return callback();
    });
    ctx.onResponseData(function(ctx, chunk, callback) {
        const transformedChunk = transformer.onChunk(chunk);
        return callback(null, transformedChunk);
    });
    ctx.onResponseEnd(function(ctx, callback) {
        try {
            const finalChunk = transformer.onFinish();
            if (finalChunk)
                ctx.proxyToClientResponse.write(finalChunk);
        } catch (ex) {
            log('>> INTERCEPTION FAILED' );
            log(ex);
        }

        return callback();
    });

    callback();
}

// Parse configuration
const configManager = new ConfigManager();
console.log('Loading configuration...');
configManager.get(function(error, config) {
    if (error) {
        console.log(error);
        return;
    }

    const filterListManager = new FilterListManager();
	config.filterListSubscriptions.forEach(function(fls) {
        filterListManager.add(fls);
	});

    console.log('Updating filter lists...');
    filterListManager.update(function(error) {
        if (error) {
            console.log(error);
            return;
        }

        console.log('Reading filter lists...');
        filterListManager.read(function(error, filterList) {
            if (error) {
                console.log(error);
                return;
            }

            console.log('Browserify-ing client side filter engine...');
            makeContentBlockerJs.generateCode(config,
                function(err, filterEngineJs) {
                    console.log('Starting proxy...');
                    const proxy = new Proxy();

                    proxy.onError(function(ctx, err) {
                        console.error('proxy error:', err);
                    });

                    proxy.onRequest(function(ctx, callback) {
                        handleProxyRequest(ctx, callback, config, filterList,
                        filterEngineJs);
                    });

                    proxy.listen({
                        port: serverDefs.port,
                    });

                    console.log('<----------------------------->');
                    console.log('< CONTENT-FILTER-POLICY PROXY >');
                    console.log('<----------------------------->');
                    console.log('Proxy running. Set your proxy to ' + ipAddress
                        + ':' + serverDefs.port + ' to use the program.');
            });
        });
    });
});
