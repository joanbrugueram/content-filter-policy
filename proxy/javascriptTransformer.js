'use strict';

const acorn = require('acorn');

const CODE_DECLAREGLOBAL =
    'var __GLOBAL__=(function () {return this || (1, eval)(\'this\');}());';
const CODE_HOOKERENTERWILDCARD =
    '__GLOBAL__.__CFPSCRIPTCTL__ ? __GLOBAL__.__CFPSCRIPTCTL__.push()' +
        ' : console.log("UNDEF __GLOBAL__.__CFPSCRIPTCTL__");\n' +
    'try {\n';
const CODE_HOOKERLEAVEWILDCARD =
    '} finally {\n' +
    '    __GLOBAL__.__CFPSCRIPTCTL__ ? __GLOBAL__.__CFPSCRIPTCTL__.pop()' +
        ' : console.log("UNDEF __GLOBAL__.__CFPSCRIPTCTL__");\n' +
    '}';
const CODE_SANDBOXENTERGLOBALSCOPEWILDCARD =
    '__GLOBAL__.__CFPSANDBOXCTL__ ? __GLOBAL__.__CFPSANDBOXCTL__.push()' +
        ' : console.log("UNDEF __GLOBAL__.__CFPSANDBOXCTL__");\n' +
    '__GLOBAL__.__ERR__ = function() {\n' +
    '    __GLOBAL__.removeEventListener("error", __GLOBAL__.__ERR__);\n' +
    '    __GLOBAL__.__CFPSANDBOXCTL__ ? __GLOBAL__.__CFPSANDBOXCTL__.pop()' +
        ' : console.log("UNDEF __GLOBAL__.__CFPSANDBOXCTL__");\n' +
    '};\n' +
    '__GLOBAL__.addEventListener("error", __GLOBAL__.__ERR__);';
const CODE_SANDBOXLEAVEGLOBALSCOPEWILDCARD =
    '__GLOBAL__.__CFPSANDBOXCTL__ ? __GLOBAL__.__CFPSANDBOXCTL__.pop()' +
        ' : console.log("UNDEF __GLOBAL__.__CFPSANDBOXCTL__");\n' +
    '__GLOBAL__.removeEventListener("error", __GLOBAL__.__ERR__);';
const CODE_SANDBOXENTERFUNCTIONSCOPEWILDCARD =
    '__GLOBAL__.__CFPSANDBOXCTL__ ? __GLOBAL__.__CFPSANDBOXCTL__.push()' +
        ' : console.log("UNDEF __GLOBAL__.__CFPSANDBOXCTL__");\n' +
    'try {\n';
const CODE_SANDBOXLEAVEFUNCTIONSCOPEWILDCARD =
    '} finally {\n' +
    '    __GLOBAL__.__CFPSANDBOXCTL__ ? __GLOBAL__.__CFPSANDBOXCTL__.pop()' +
        ' : console.log("UNDEF __GLOBAL__.__CFPSANDBOXCTL__");\n' +
    '}';
/**
 * Insert hooking/sandboxing code into a Javascript string.
 * @param  {String} body             Javascript string to transform.
 * @param  {Object} options    Options for script transformation:
 * - enterContext    Enter script context on every function.
 * - sandbox         Add code to sandbox script actions.
 * @return {String}                  Transformed Javascript string.
 */
function hookJavascriptString(body, options) {
    const patches = [];

    let checkingStrictModeGlobal = true;
    let checkingStrictModeFunction = false;
    let nextScopeIsFn = false;
    let stackLevel = [];

    const codeStartGlobalScope = CODE_DECLAREGLOBAL + '\n' +
        (options.sandbox ? CODE_SANDBOXENTERGLOBALSCOPEWILDCARD + '\n' : '') +
        (options.enterContext ? CODE_HOOKERENTERWILDCARD + '\n' : '');
    const codeLeaveGlobalScope =
        '\n' + // Avoid getting tricked by a line-comment at the last line
        (options.enterContext ? CODE_HOOKERLEAVEWILDCARD + '\n' : '') +
        (options.sandbox ? CODE_SANDBOXLEAVEGLOBALSCOPEWILDCARD + '\n' : '');
    const codeStartFunctionScope = CODE_DECLAREGLOBAL + '\n' +
        (options.sandbox ? CODE_SANDBOXENTERFUNCTIONSCOPEWILDCARD + '\n' : '') +
        (options.enterContext ? CODE_HOOKERENTERWILDCARD + '\n' : '');
    const codeLeaveFunctionScope =
        (options.enterContext ? CODE_HOOKERLEAVEWILDCARD + '\n' : '') +
        (options.sandbox ? CODE_SANDBOXLEAVEFUNCTIONSCOPEWILDCARD + '\n' : '');

    const tokens = acorn.tokenizer(body);
    for (let token = tokens.getToken();
        true;
        token = tokens.getToken()) {
        if (checkingStrictModeGlobal) {
            const isStrictMode = (token.type === acorn.tokTypes.string &&
                token.value == 'use strict');
            patches.push({
                position: token.start,
                code: (isStrictMode ? '"use strict";' : '') +
                    codeStartGlobalScope,
            });
            checkingStrictModeGlobal = false;
        } else if (checkingStrictModeFunction) {
            const isStrictMode = (token.type === acorn.tokTypes.string &&
                token.value == 'use strict');

            patches.push({
                position: token.start,
                code: (isStrictMode ? '"use strict";' : '') +
                    codeStartFunctionScope,
            });
            checkingStrictModeFunction = false;
        }

        if (token.type === acorn.tokTypes.eof)
            break;

        if (token.type == acorn.tokTypes._function) {
            nextScopeIsFn = true;
        } else if (token.type === acorn.tokTypes.braceL) {
            if (nextScopeIsFn === true) {
                checkingStrictModeFunction = true;
            }
            stackLevel.push(nextScopeIsFn);
            nextScopeIsFn = false;
        } else if (token.type === acorn.tokTypes.braceR) {
            if (stackLevel.length === 0) {
                console.log('UNBALANCED {}? Unsupported.');
                return body;
            }
            const lastScopeIsFn = stackLevel.pop();
            if (lastScopeIsFn === true) {
                patches.push({
                    position: token.start,
                    code: codeLeaveFunctionScope,
                });
            }
        }
    }
    patches.push({
        position: body.length,
        code: codeLeaveGlobalScope,
    });

    // Create patched string
    let patchedString = '';
    let currentPos = 0;
    patches.forEach(function(patch) {
        patchedString += body.substring(currentPos, patch.position);
        patchedString += patch.code;
        currentPos = patch.position;
    });
    patchedString += body.substring(currentPos, body.length);

    return patchedString;
}

/**
 * Transform a Javascript script, injecting the code necessary to
 * trace scripts / block ads according to the configuration.
 * @param  {String} javascriptString String containing the script to transform.
 * @param  {Object} options    Options for script transformation:
 * - enterContext    Enter script context on every function.
 * - sandbox         Add code to sandbox script actions.
 * - wrapCode        Wrap code in function before transforming.
 *                   (Used to transform error handlers)
 * @return {String}                  String containing the transformed script.
 */
function transformJavascriptString(javascriptString, options) {
    if (isNullTransform(options)) {
        console.log('WARNING: Transforming Javascript with null transform.');
    }

    // If requested, wrap the code in a function before parsing
    // This is used so statements like 'return false;' that can't ocurr
    // outside a function can be successfully transformed (e.g. event handlers)
    const wrapJavascriptString = options.wrapCode
        ? 'function __BODYWRAPPER__() {' +
            javascriptString +
            '}/*__BODYWRAPPEREND__*/'
        : javascriptString;

    // Transform code to add base hooks.
    const hookedBodyCode = hookJavascriptString(wrapJavascriptString, options);

    // Unwrap wrapped code
    const transformedBodyCode = options.wrapCode ? hookedBodyCode.replace(
        /function __BODYWRAPPER__\(\) {([\s\S]+)}\/\*__BODYWRAPPEREND__\*\//gm,
        '$1') : hookedBodyCode;
    return transformedBodyCode;
}

/**
 * Checks if the specified transform options do not require any change to the
 * script.
 * @param  {Object} options    Options for script transformation:
 * - enterContext    Enter script context on every function.
 * - sandbox         Add code to sandbox script actions.
 * @return {Boolean}         true if the specified options
 *                           don't require any change to the script.
 */
function isNullTransform(options) {
    return !options.enterContext && !options.sandbox;
}

/**
 * Checks if the given MIME type is a Javascript script mime type.
 * @param  {String}  mimeType The MIME type to check.
 * @return {Boolean}          true if the given MIME type is a Javascript MIME.
 */
function isJavascriptMimeType(mimeType) {
    const mimeTypeTrim = mimeType.trim();

    return mimeTypeTrim ==='text/javascript' ||
     mimeTypeTrim === 'application/javascript' ||
     mimeTypeTrim === 'application/x-javascript';
}

module.exports = {
    transformJavascriptString: transformJavascriptString,
    isNullTransform: isNullTransform,
    isJavascriptMimeType: isJavascriptMimeType,
};
