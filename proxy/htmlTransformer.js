'use strict';

/**
 * Create a new transformer for HTML documents.
 * @param {Boolean} bootstrapCode Content filter Javascript bootstrap code.
 */
function HtmlTransformer(bootstrapCode) {
    this.isFirstChunk = true;

    this.onChunk = function(chunk) {
        if (this.isFirstChunk === true) {
            this.isFirstChunk = false;

            return Buffer.concat([
                Buffer.from('<script type="text/javascript">' +
                    bootstrapCode + '</script>', 'utf8'),
                chunk,
            ]);
        }

        return chunk;
    };

    this.onFinish = function() {
        return null;
    };
}

/**
 * Checks if the given MIME type is a HTML mime type.
 * @param  {String}  mimeType The MIME type to check.
 * @return {Boolean}          true if the given MIME type is a HTML MIME.
 */
function isHtmlMimeType(mimeType) {
    const mimeTypeTrim = mimeType.trim();

    return mimeTypeTrim ==='text/html';
}

module.exports = {
    HtmlTransformer: HtmlTransformer,
    isHtmlMimeType: isHtmlMimeType,
};
