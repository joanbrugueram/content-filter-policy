'use strict';

const ABPFilterParser = require('abp-filter-parser');

/**
  * Generate a CSS style sheet, assiging custom CSS variables
  * (--filtered:true|false) to every CSS selector filter in the parsed
  * filter list.
 * @param  {Structure} parsedFilterData Parsed AdBlock filter lists.
 * @param  {String} domain              Domain for which to generate the CSS.
 * @return {String}                     Generated CSS style sheet.
 */
function generateStylesheetFromFilters(parsedFilterData, domain) {
    const filterSelectors = [];
    const exceptionSelectors = [];

    parsedFilterData.htmlRuleFilters.forEach(function(filter) {
        const appliesToDomain = ABPFilterParser.matchesFilter(filter, null, {
            domain: domain,
        });

        if (appliesToDomain) {
            Array.prototype.push.call(!filter.isException ? filterSelectors
                : exceptionSelectors, filter.htmlRuleSelector);
        }
    });

    const BLOCK_SIZE = 100;
    let styleSheet = '';
    if (filterSelectors.length > 0) {
        for (let i = 0; i < filterSelectors.length; i += BLOCK_SIZE) {
            const filterBlock = filterSelectors.slice(i, i + BLOCK_SIZE);
            styleSheet += filterBlock.join(',') + '\n{--filtered:true;}';
        }
    }
    if (exceptionSelectors.length > 0) {
        for (let i = 0; i < exceptionSelectors.length; i += BLOCK_SIZE) {
            const exceptionBlock = exceptionSelectors.slice(i, i + BLOCK_SIZE);
            styleSheet += exceptionBlock.join(',') + '\n{--filtered:false;}';
        }
    }

    return styleSheet;
};

module.exports = {generateStylesheetFromFilters: generateStylesheetFromFilters};
