const constants = {
    injectStyleSheetName: '__CONTENTFILTERSELECTORS__.css',
    injectJavascriptName: '__INJECTJAVASCRIPT__.js',
    generateStatusCodePath: '__GENERATESTATUSCODE__',
    checkRequestPath: '__CHECKREQUEST__',
    requestPolicy: {
        sandbox: 'sandbox',
        blank: 'blank',
        notFound: 'notFound',
    },
    changeContext: {
        onEnterScript: 'onEnterScript',
        onMemberAccess: 'onMemberAccess',
    },
    detectSandbox: {
        onEnterScript: 'onEnterScript',
        whenRequired: 'whenRequired',
    },
    port: 8081,
};

module.exports = constants;
