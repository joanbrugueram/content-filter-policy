(function() {
    // Configuration is written here when the script is executed
    const config = '@@INJECT_CONFIG_HERE@@';

    const javascriptTransformer = require('./javascriptTransformer');
    const backgroundImageParser = require('./backgroundImageParser');
    const serverDefs = require('./serverDefs');
    const stackTrace = require('stacktrace-js');

    let excludeHookZoneCtr = 0;

    /**
     * Injects the CSS style sheet required by the filter engine to work
     * (defines the filters over the HTML elements).
     */
    function ContentFilterCssInjector() {
        const contentFilterCssXhr = new XMLHttpRequest();
        contentFilterCssXhr.open('GET', serverDefs.injectStyleSheetName,
            false);
        contentFilterCssXhr.send(null);

        this.injectCssIntoDocument = function(doc, finishCb) {
            if (contentFilterCssXhr.status === 200) {
                if (doc.head) {
                    const style = doc.createElement('style');
                    style.innerHTML = contentFilterCssXhr.responseText;
                    doc.head.insertBefore(style, doc.head.firstChild);
                } else {
                    doc.write('<style>' + contentFilterCssXhr.responseText +
                        '</style>');
                }

                finishCb(null);
            } else {
                finishCb(new Error('Can not load content filter CSS.'), null);
            }
        };
    };

    /**
     * Keeps track of whether or not the currently executing script is inside
     * some kind of restricted context (script context or sandbox context).
     */
    function ContentPolicyContextController() {
        let contextLevel = 0;
        const enterContextListeners = new Set();
        const leaveContextListeners = new Set();

        this.registerEnterListener = function(listener) {
            enterContextListeners.add(listener);
        };

        this.unregisterEnterListener = function(listener) {
            enterContextListeners.delete(listener);
        };

        this.triggerEnterListeners = function() {
            enterContextListeners.forEach(function(listener) {
                listener();
            });
        };

        this.registerLeaveListener = function(listener) {
            leaveContextListeners.add(listener);
        };

        this.unregisterLeaveListener = function(listener) {
            leaveContextListeners.delete(listener);
        };

        this.triggerLeaveListeners = function() {
            leaveContextListeners.forEach(function(listener) {
                listener();
            });
        };

        this.push = function() {
            if (contextLevel === 0) {
                this.triggerEnterListeners();
            }
            contextLevel++;
        };

        this.pop = function() {
            contextLevel--;
            if (contextLevel === 0) {
                this.triggerLeaveListeners();
            }
        };

        this.isActive = function() {
            return contextLevel !== 0;
        };
    }

    /**
     * Main object that creates and initializes the filter policy scripts.
     * @param {Window} whichWindow       Window over which to initialize the
     *                                   script.
     * @param {Map} cfpRegistry          Map over which to register
     *                                   the class instance, for later access.
     * @param {Object} scriptContextCtl  Script context controller.
     * @param {Object} sandboxContextCtl Sandbox context controller.
     */
    function ContentFilterPolicy(whichWindow, cfpRegistry,
        scriptContextCtl, sandboxContextCtl) {
        const whichDocument = whichWindow.document;

        console.log('STARTING CFP ON WINDOW FOLLOWING:');
        console.log(whichWindow);

        const filterPolicies = [];
        const filteredElements = new Set();
        const sandboxedElements = new Set();
        const bannedSourceCache = {};

        /**
         * Returns whether we are in a sandbox context or not.
         * @return {Boolean} true if we are in a sandbox context,
         *                   false otherwise.
         */
        function isInSandbox() {
            if (config.detectSandbox ===
                serverDefs.detectSandbox.whenRequired) {
                const stackFrames = stackTrace.getSync();
                return stackFrames.some(function(sf) {
                    return isSourceBanned(sf.fileName);
                });
            } else if (config.detectSandbox ===
                serverDefs.detectSandbox.onEnterScript) {
                return sandboxContextCtl.isActive();
            }
        }

        const policyApi = {
            inSandbox: function() {
                return excludeHookZoneCtr === 0 &&
                    wrapFnExcludeHookZone(isInSandbox)();
            },
            isFilteredElement: function(object) {
                return filteredElements.has(object);
            },
            whichWindow: function() {
                return whichWindow;
            },
            wrapFnExcludeHookZone: wrapFnExcludeHookZone,
        };

        /**
         * Checks if the URL of a resource is banned by a filter.
         * @param  {String}  sourceUrl   The source URL of the resource.
         * @return {Boolean}             true if the resource is banned,
         *                               false otherwise.
         */
        function isSourceBanned(sourceUrl) {
            if (!bannedSourceCache.hasOwnProperty(sourceUrl)) {
                const checkBannedSourceXhr = new XMLHttpRequest();
                checkBannedSourceXhr.open('GET', serverDefs.checkRequestPath +
                    '?url=' + encodeURIComponent(sourceUrl),
                    false /* Synchronous request */);
                checkBannedSourceXhr.send(null);
                bannedSourceCache[sourceUrl] =
                    checkBannedSourceXhr.responseText === 'true';
            }

            return bannedSourceCache[sourceUrl];
        }

        /**
         * Checks whether an element fits the criteria to be filtered or not.
         * @param  {Element} elm The element to check.
         * @return {Boolean}     true if the element should be blocked,
         *                       false otherwise.
         */
        function shouldElementBeFiltered(elm) {
            if ((elm.tagName == 'IMG' && isSourceBanned(elm.src))) {
                return true;
            }

            const elementStyle = originalWindowGetComputedStyle(elm);

            if (elementStyle.backgroundImage) {
                const urls = backgroundImageParser.parseBackgroundImageUrls(
                    elementStyle.backgroundImage);
                if (urls.some(function(url) {
                    return isSourceBanned(url);
                })) {
                    return true;
                }
            }

            return (
                elementStyle.getPropertyValue('--filtered').trim() === 'true'
                || sandboxedElements.has(elm));
        }

        const hookedPrototypes = new Set();

        /**
         * Wraps a function in a way that it doesn't trigger the script context
         * even if it accesses any of the hooked prototype properties.
         * @param  {Function} fn The function to wrap.
         * @return {Function}      Wrapped function instance (same args/return).
         */
        function wrapFnExcludeHookZone(fn) {
            return function(...args) {
                excludeHookZoneCtr++;
                try {
                    return fn.apply(this, args);
                } finally {
                    excludeHookZoneCtr--;
                }
            };
        }

        /**
         * Called when a function that potentially causes a switch to the script
         * context is called.
         * @param  {Element} elm             DOM element associated to the call.
         * @param  {String} originalPropName (For debugging only) Name of the
         *                                   accessed method/property.
         */
        function onElementAccess(elm, originalPropName) {
            if (!scriptContextCtl.isActive() &&
                excludeHookZoneCtr === 0) {
                scriptContextCtl.push();
                console.log('Access: ' + originalPropName);

                // Schedule restoring the script context in the next microtask
                // https://jakearchibald.com/2015/tasks-microtasks-queues-and-schedules/
                Promise.resolve().then(function() {
                    scriptContextCtl.pop();
                });
            }
        }

        /**
         * Wraps a function call, checking whether a switch to the script
         * context is necessary.
         * @param  {Function} originalFunction The function to wrap.
         * @param  {String} originalPropName (For debugging only) Name of the
         *                                   accessed method/property.
         * @return {Function}      Wrapped function instance (same args/return).
         */
        function wrapElementFunctionCall(originalFunction, originalPropName) {
            return function(...args) {
                onElementAccess(this, 'Element.' + originalPropName);
                return originalFunction.apply(this, args);
            };
        }

        /**
         * Hooks all methods in the given object prototype, and all its base
         * prototypes, triggering the script context if any of them is called.
         * @param  {Prototype} prototype The prototype to wrap.
         */
        function hookPrototype(prototype) {
            const name = prototype.constructor.name;
            if (!hookedPrototypes.has(name)) {
                whichWindow.Object.getOwnPropertyNames(prototype).forEach(
                    function(prop) {
                    // Inspired by http://stackoverflow.com/a/33064438
                    const propDesc = whichWindow.Object.
                        getOwnPropertyDescriptor(prototype, prop);
                    if (propDesc && propDesc.configurable &&
                        propDesc.writable !== undefined) {
                        if (typeof propDesc.value === 'function' &&
                            prop !== 'constructor') {
                            whichWindow.Object.defineProperty(
                                prototype, prop, {
                                writable: propDesc.writable,
                                value: wrapElementFunctionCall(propDesc.value,
                                    prop),
                                enumerable: propDesc.enumerable,
                                configurable: propDesc.configurable,
                            });
                        }
                    } else if (propDesc && propDesc.configurable) {
                        const newGet = propDesc.get !== undefined
                            ? wrapElementFunctionCall(propDesc.get, prop)
                            : undefined;
                        const newSet = propDesc.set !== undefined
                            ? wrapElementFunctionCall(propDesc.set, prop)
                            : undefined;

                        whichWindow.Object.defineProperty(
                            prototype, prop, {
                            get: newGet,
                            set: newSet,
                            enumerable: propDesc.enumerable,
                            configurable: propDesc.configurable,
                        });
                    }
                });

                hookedPrototypes.add(name);

                const parentPrototype = Object.getPrototypeOf(prototype);
                if (parentPrototype !== null)
                    hookPrototype(parentPrototype);
            }
        }

        /**
         * Checks for necessary filter operations over a new
         * DOM node after it has been added to the document.
         * (Executes in the next microtask after the element has been added).
         * @param  {Element} elm The element to check.
         */
        function checkNodeAddedDelayed(elm) {
            if (elm.tagName === 'IFRAME' && elm.src === '') {
                // We have to wait until the (empty) IFRAME
                // loads, because otherwise the window isn't
                // created yet
                const loadFunc = wrapFnExcludeHookZone(function() {
                    elm.contentWindow.__CFPSCRIPTCTL__ =
                        window.__CFPSCRIPTCTL__;
                    elm.contentWindow.__CFPSANDBOXCTL__ =
                        window.__CFPSANDBOXCTL__;
                    new window.__CFPCLASS__(elm.contentWindow,
                        window.__CFP__,
                        window.__CFPSCRIPTCTL__,
                        window.__CFPSANDBOXCTL__);
                    elm.removeEventListener('load', loadFunc);
                });
                elm.addEventListener('load', loadFunc);
            }

            const jsTransformOpts = {
                enterContext: config.changeContext ===
                    serverDefs.changeContext.onEnterScript,
            };

            if (!javascriptTransformer.isNullTransform(jsTransformOpts)) {
                if (elm.tagName == 'SCRIPT') {
                    if (elm.type === undefined || elm.type === '' ||
                    javascriptTransformer.isJavascriptMimeType(elm.type)) {
                        elm.innerHTML = javascriptTransformer.
                            transformJavascriptString(elm.innerHTML,
                                jsTransformOpts);
                    }
                }

                // This is a bit hacky, but it should handle pretty much all
                // event handlers without needing to list them explicitly
                [].forEach.call(elm.attributes, function(attr) {
                    if (attr.nodeName.startsWith('on')) {
                        const scriptText = elm.getAttribute(attr.nodeName);
                        const newScriptText = javascriptTransformer.
                            transformJavascriptString(scriptText, Object.assign(
                                jsTransformOpts, {wrapCode: true}));
                        elm.setAttribute(attr.nodeName, newScriptText);
                    }
                });
            }
        }
        /**
         * Updates the tracking status of a DOM node after is has changed.
         * @param  {Element} elm     The element to check.
         * @param  {Boolean} removed true if the element is being removed,
         *                           false otherwise.
         */
        function checkDomNode(elm, removed) {
            if (config.changeContext ===
                serverDefs.changeContext.onMemberAccess) {
                hookPrototype(Object.getPrototypeOf(elm));
            }

            const wasFiltered = filteredElements.has(elm);
            const isFiltered = shouldElementBeFiltered(elm) && !removed;

            if (!wasFiltered && isFiltered) {
                filteredElements.add(elm);

                filterPolicies.forEach(function(policy) {
                    if (policy.instance.onFilteredElementAdded)
                        policy.instance.onFilteredElementAdded(elm);
                });

                if (scriptContextCtl.isActive()) {
                    filterPolicies.forEach(function(policy) {
                        if (policy.instance.onEnterScriptContext)
                            policy.instance.onEnterScriptContext(elm);
                    });
                }
            } else if (wasFiltered && !isFiltered) {
                if (scriptContextCtl.isActive()) {
                    filterPolicies.forEach(function(policy) {
                        if (policy.instance.onLeaveScriptContext)
                            policy.instance.onLeaveScriptContext(elm);
                    });
                }

                filterPolicies.forEach(function(policy) {
                    if (policy.instance.onFilteredElementRemoved)
                        policy.instance.onFilteredElementRemoved(elm);
                });

                filteredElements.delete(elm);
                if (sandboxedElements.has(elm))
                    sandboxedElements.delete(elm);
            }
        }

        const observer = new whichWindow.MutationObserver(
            wrapFnExcludeHookZone(function(mutations) {
            mutations.forEach(function(mutation) {
                if (mutation.type === 'attributes') {
                    checkDomNode(mutation.target, false, false);
                } else if (mutation.type === 'childList') {
                    [].forEach.call(mutation.addedNodes, function(node) {
                        if (node instanceof whichWindow.Element) {
                            checkNodeAddedDelayed(node);
                            checkDomNode(node, false, false);
                        }
                    });
                    [].forEach.call(mutation.removedNodes, function(node) {
                        if (node instanceof whichWindow.Element) {
                            checkDomNode(node, true, false);
                        }
                    });
                }
            });
        }));

        /**
         * Checks for necessary filter operations over a new
         * DOM node instantly after it has been added to the document
         * (keeps the original call stack).
         * @param  {Element} elm The element to check.
         */
        function checkNodeAddedRealTime(elm) {
            if (isInSandbox()) {
                sandboxedElements.add(elm);
            }
        }

        // Use the deprecated DOM mutation events, since they are called
        // instantaneously, and therefore they let us check the sandbox state
        const onDomNodeInserted = wrapFnExcludeHookZone(function(ev) {
            const elm = ev.target;
            checkNodeAddedRealTime(elm);
        });

        /**
         * Wraps a function call, watching for changes in the document state
         * (creating a new document / writing elements directly to it)
         * in order to detect new elements or reinject the script to a new page.
         * @param  {Function} originalCall Original function reference.
         * @param  {Object} originalThis Original function 'this' binding.
         * @param  {Array} args         Original function arguments.
         * @return {Object}              Original function return value.
         */
        function checkDocumentChange(originalCall, originalThis, args) {
            let original;
            const reload = (whichDocument.readyState === 'complete');
            if (reload) {
                onShutdownContentFilterPolicy();
            } else {
                original = new Set([].slice.call(
                   whichDocument.querySelectorAll('*')));
            }
            const ret = originalCall.apply(originalThis, args);
            if (reload) {
                new window.__CFPCLASS__(whichWindow,
                    window.__CFP__,
                    window.__CFPSCRIPTCTL__,
                    window.__CFPSANDBOXCTL__);
            } else {
                const after = new Set([].slice.call(
                    whichDocument.querySelectorAll('*')));
                after.forEach(function(elm) {
                    if (!original.has(elm)) {
                        checkNodeAddedRealTime(elm);
                    }
                });
            }
            return ret;
        }

        const originalDocumentOpen = whichDocument.open;
        const originalDocumentWrite = whichDocument.write;
        const originalDocumentWriteLn = whichDocument.writeln;
        const originalWindowGetComputedStyle = whichWindow.getComputedStyle;

        // Script context enter/exit
        const notifyPolicyEnterScriptContext = wrapFnExcludeHookZone(
            function() {
            filteredElements.forEach(function(elm) {
                filterPolicies.forEach(function(policy) {
                    if (policy.instance.onEnterScriptContext)
                        policy.instance.onEnterScriptContext(elm);
                });
            });
        });

        const notifyPolicyLeaveScriptContext = wrapFnExcludeHookZone(
            function() {
            filteredElements.forEach(function(elm) {
                filterPolicies.forEach(function(policy) {
                    if (policy.instance.onLeaveScriptContext)
                        policy.instance.onLeaveScriptContext(elm);
                });
            });
        });

        (function() {
            /**
             * Registers a new content filter policy.
             * @param  {Object} instance Instance of the filter policy.
             * @param  {Structure} params   Parameters of the filter policy.
             */ // eslint-disable-next-line no-unused-vars
            function registerFilterPolicy(instance, params) {
                filterPolicies.push({instance: instance, params: params});
            }

            // Filter policies are written here when the script is executed
            '@@INJECT_POLICIES_HERE@@';
        })();

        contentFilterCssInjector.injectCssIntoDocument(whichDocument,
            onStartContentFilterPolicy);

        /**
         * Starts the content filter on the current window.
         * @param {Error} err       Error injecting the content filter CSS.
         */
        function onStartContentFilterPolicy(err) {
            if (err) {
                console.log(err);
                return;
            }

            filterPolicies.forEach(function(policy) {
                if (policy.instance.onStart)
                    policy.instance.onStart(policyApi, policy.params);
            });

            // Detect elements that have the '--filtered:true' custom
            // CSS variable, from the injected CSS style sheet
            [].forEach.call(whichDocument.querySelectorAll('*'), function(elm) {
                checkDomNode(elm, false);
            });

            // Start mutation observer
            observer.observe(whichDocument, {
                attributes: true,
                childList: true,
                subtree: true,
            });

            whichDocument.addEventListener('DOMNodeInserted',
                onDomNodeInserted, false);

            whichDocument.open = wrapFnExcludeHookZone(function(...args) {
                checkDocumentChange(originalDocumentOpen, this, args);
            });

            whichDocument.write = wrapFnExcludeHookZone(function(...args) {
                checkDocumentChange(originalDocumentWrite, this, args);
            });

            whichDocument.writeln = wrapFnExcludeHookZone(function(...args) {
                checkDocumentChange(originalDocumentWriteLn, this, args);
            });

            if (config.changeContext ===
                serverDefs.changeContext.onMemberAccess) {
                whichWindow.getComputedStyle = function(...args) {
                    onElementAccess(args[0], 'Window.getComputedStyle');
                    return originalWindowGetComputedStyle.apply(this, args);
                };
            }

            scriptContextCtl.registerEnterListener(
                notifyPolicyEnterScriptContext);
            scriptContextCtl.registerLeaveListener(
                notifyPolicyLeaveScriptContext);

            cfpRegistry.set(whichWindow, this);
        }

        /**
         * Stops the content filter on the current window.
         */
        function onShutdownContentFilterPolicy() {
            scriptContextCtl.unregisterEnterListener(
                notifyPolicyEnterScriptContext);
            scriptContextCtl.unregisterLeaveListener(
                notifyPolicyLeaveScriptContext);

            filteredElements.forEach(function(elm) {
                filterPolicies.forEach(function(policy) {
                    if (policy.instance.onFilteredElementRemoved)
                        policy.instance.onFilteredElementRemoved(elm);
                });
            });

            filterPolicies.forEach(function(policy) {
                if (policy.instance.onStop)
                    policy.instance.onStop();
            });

            whichDocument.open = originalDocumentOpen;
            whichDocument.writeln = originalDocumentWriteLn;
            whichDocument.write = originalDocumentWrite;
            whichWindow.getComputedStyle = originalWindowGetComputedStyle;

            whichDocument.removeEventListener('DOMNodeInserted',
                onDomNodeInserted, false);
            observer.disconnect();

            cfpRegistry.delete(whichWindow);
        };
    }

    // Set up global variables for all windows
    if (window.__CFP__ !== undefined) {
        throw new Error('Reinjected __CFP__ despite bootstraper?');
    }

    window.__CFPSCRIPTCTL__ =
        new ContentPolicyContextController();
    window.__CFPSANDBOXCTL__ =
        new ContentPolicyContextController();
    window.__CFPCLASS__ = ContentFilterPolicy;
    window.__CFP__ = new Map();

    const contentFilterCssInjector = new ContentFilterCssInjector();

    // Start ContentFilterPolicy on top window
    new window.__CFPCLASS__(window,
        window.__CFP__,
        window.__CFPSCRIPTCTL__,
        window.__CFPSANDBOXCTL__);
})();
