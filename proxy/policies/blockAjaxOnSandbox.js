/*
 * Hook all AJAX accesses to filter them if inside a sandboxed script.
 */
registerFilterPolicy({
    /**
     *
     * @param  {Integer} returnCode The return code to return for the request.
     */
    hookAjax: function(returnCode) {
        const self = this;
        const whichWindow = this.policyApi.whichWindow();
        this.originalOpen = whichWindow.XMLHttpRequest.prototype.open;
        whichWindow.XMLHttpRequest.prototype.open = function(...args) {
            if (self.policyApi.inSandbox() && typeof args[1] === 'string') {
                console.log('Blocked AJAX on sandbox to URL: ' + args[1]);
                args[1] = serverDefs.generateStatusCodePath +
                    '?code=' + (returnCode || 404);
            }
            self.originalOpen.apply(this, args);
        };
    },

    /**
     * Hook all AJAX accesses to filter them if inside a sandboxed script.
     * @param {Object} policyApi Instance of content filter policy API.
     * @param {Object} policyParams Object containing policy parameters.
     * - return: The return code to return for the request.
     */
    onStart: function(policyApi, policyParams) {
        this.policyApi = policyApi;
        this.hookAjax(policyParams.return);
    },

	onStop: function() {
        const whichWindow = this.policyApi.whichWindow();
        whichWindow.XMLHttpRequest.prototype.open = this.originalOpen;
	},
}, '@@INJECT_POLICY_PARAMETERS_HERE@@');
