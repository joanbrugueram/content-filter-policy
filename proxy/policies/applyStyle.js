/*
 * Apply CSS style over filtered elements.
 */
registerFilterPolicy({
    changeShadowStyleApplies: function(elm, eventName, newApplies) {
        const elmProps = this.filteredElementProperties.get(elm);

        if (!elmProps.shadowStyleApplies[eventName] && newApplies) {
            elmProps.shadowStyleApplies[eventName] = true;
            this.saveToShadowStyleAndApplyPolicyStyle(elm, eventName);
        } else if (elmProps.shadowStyleApplies[eventName] && !newApplies) {
            this.restoreFromShadowStyle(elm, eventName);
            elmProps.shadowStyleApplies[eventName] = false;
        }
    },

    saveToShadowStyleAndApplyPolicyStyle: function(elm, eventName) {
        if (!this.policyParams[eventName])
            return;

        const elmProps = this.filteredElementProperties.get(elm);

        Object.keys(this.policyParams[eventName]).forEach(function(styleName) {
            elmProps.shadowStyle[styleName] = {
                value: elm.style.getPropertyValue(styleName),
                priority: elm.style.getPropertyPriority(styleName),
            };

            elm.style.removeProperty(styleName); // Required MsEdge15
            elm.style.setProperty(styleName,
                this.policyParams[eventName][styleName], 'important');
        }.bind(this));
    },

    restoreFromShadowStyle: function(elm, eventName) {
        if (!this.policyParams[eventName])
            return;

        const elmProps = this.filteredElementProperties.get(elm);

        Object.keys(this.policyParams[eventName]).forEach(function(styleName) {
            elm.style.removeProperty(styleName); // Required MsEdge15
            elm.style.setProperty(styleName,
                elmProps.shadowStyle[styleName].value,
                elmProps.shadowStyle[styleName].priority);
        });
    },

    onFilteredElementAdded: function(elm) {
        const elmProps = {};
        this.filteredElementProperties.set(elm, elmProps);
        elmProps.shadowStyle = {};
        elmProps.shadowStyleApplies = {
            'always': false,
            'nohover': false,
            'onhover': false,
            'nofirstclick': false,
            'onfirstclick': false,
        };
        if (this.policyParams['onhover'] || this.policyParams['nohover']) {
            elmProps.mouseOver = this.policyApi.wrapFnExcludeHookZone(
                function() {
                this.changeShadowStyleApplies(elm, 'nohover', false);
                this.changeShadowStyleApplies(elm, 'onhover', true);
            }).bind(this);
            elm.addEventListener('mouseover', elmProps.mouseOver);
            elmProps.mouseLeave = this.policyApi.wrapFnExcludeHookZone(
                function() {
                this.changeShadowStyleApplies(elm, 'onhover', false);
                this.changeShadowStyleApplies(elm, 'nohover', true);
            }).bind(this);
            elm.addEventListener('mouseleave', elmProps.mouseLeave);
        }
        if (this.policyParams['onfirstclick'] ||
            this.policyParams['nofirstclick']) {
            elmProps.click = this.policyApi.wrapFnExcludeHookZone(function() {
                this.changeShadowStyleApplies(elm, 'nofirstclick', false);
                this.changeShadowStyleApplies(elm, 'onfirstclick', true);
            }).bind(this);
            elm.addEventListener('click', elmProps.click);
        }

        this.changeShadowStyleApplies(elm, 'always', true);
        this.changeShadowStyleApplies(elm, 'nohover', true);
        this.changeShadowStyleApplies(elm, 'nofirstclick', true);

        console.log('REFRESH_SHADOW_STYLE:' +
            ' of elm with tag=' + elm.tagName +
            ',id=' + elm.id +
            ',classes=' + elm.className);
    },

    onFilteredElementRemoved: function(elm) {
        const elmProps = this.filteredElementProperties.get(elm);

        Object.keys(elmProps.shadowStyleApplies).forEach(function(eventName) {
            this.changeShadowStyleApplies(elm, eventName, false);
        }.bind(this));

        if (elmProps.mouseOver) {
            elm.removeEventListener('mouseover', elmProps.mouseOver);
        }
        if (elmProps.mouseLeave) {
            elm.removeEventListener('mouseleave', elmProps.mouseLeave);
        }
        if (elmProps.click) {
            elm.removeEventListener('click', elmProps.click);
        }

        this.filteredElementProperties.delete(elm);
    },

    onEnterScriptContext: function(elm) {
        const elmProps = this.filteredElementProperties.get(elm);

        Object.keys(elmProps.shadowStyleApplies).forEach(function(eventName) {
            if (elmProps.shadowStyleApplies[eventName]) {
                this.restoreFromShadowStyle(elm, eventName);
            }
        }.bind(this));
    },

    onLeaveScriptContext: function(elm) {
        const elmProps = this.filteredElementProperties.get(elm);

        Object.keys(elmProps.shadowStyleApplies).forEach(function(eventName) {
            if (elmProps.shadowStyleApplies[eventName]) {
                this.saveToShadowStyleAndApplyPolicyStyle(elm, eventName);
            }
        }.bind(this));
    },

    /** Start policy object
    * @param {Object} policyApi Instance of content filter policy API.
    * @param  {Object} policyParams Accepted parameters:
    * - event: always || onhover || nohover || onfirstclick || nofirstclick.
    *          always: Always applies.
    *          always: Always applies.
    *          always: Always applies.
    * - onhover: Whether to only apply the style when the element is hovered,
    *            or not hovered. Accepted values:
    *   * null: Apply always.
    *   * false: Only apply when not hovered.
    *   * true: Only apply when hovered.
    * - onfirstclick: Whether to only apply the style after the first click.
    *                 Accepted values:
    *   * null: Apply always.
    *   * false: Only apply when not clicked once.
    *   * true: Only apply when clicked once.
    * The rest of the parameters will be applied as inline styles.
    */
    onStart: function(policyApi, policyParams) {
        this.policyApi = policyApi;
        this.policyParams = policyParams;
		this.filteredElementProperties = new Map();
    },
}, '@@INJECT_POLICY_PARAMETERS_HERE@@');
