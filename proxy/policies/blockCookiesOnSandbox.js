/*
 * Hook all cookies accesses to filter them if inside a sandboxed script.
 */
registerFilterPolicy({
    /**
     * Hook all cookies accesses to filter them if inside a sandboxed script.
     */
    hookCookiesAccess: function() {
        const self = this;
        const whichWindow = this.policyApi.whichWindow();

        // Inspired by http://stackoverflow.com/a/33064438
        this.cookieDesc =
            whichWindow.Object.getOwnPropertyDescriptor(
                whichWindow.Document.prototype, 'cookie') ||
            whichWindow.Object.getOwnPropertyDescriptor(
                whichWindow.HTMLDocument.prototype, 'cookie');
        if (this.cookieDesc && this.cookieDesc.configurable) {
            whichWindow.Object.defineProperty(whichWindow.document, 'cookie', {
                get: function() {
                    if (self.policyApi.inSandbox() > 0) {
                        console.log('Blocked GET COOKIE on sandbox');
                        return '';
                    }
                    return self.cookieDesc.get.call(this);
                },
                set: function(val) {
                    if (self.policyApi.inSandbox() > 0) {
                        console.log('Blocked SET COOKIE on sandbox: ' + val);
                        return;
                    }
                    self.cookieDesc.set.call(this, val);
                },
                configurable: true,
            });
        } else {
            console.log('Error overriding cookie (block cookies on sandbox).');
        }
    },

    /**
     * Start policy callback.
     * @param {Object} policyApi Instance of content filter policy API.
     * @param {Object} policyParams Object containing policy parameters.
     * - return: The return code to return for the request.
     */
    onStart: function(policyApi) {
        this.policyApi = policyApi;
        this.hookCookiesAccess();
    },

    onStop: function() {
        const whichWindow = this.policyApi.whichWindow();
        if (this.cookieDesc && this.cookieDesc.configurable) {
            whichWindow.Object.defineProperty(whichWindow.document, 'cookie', {
                get: this.cookieDesc.get,
                set: this.cookieDesc.set,
            });
        }
    },
}, '@@INJECT_POLICY_PARAMETERS_HERE@@');
