/*
 * Avoid access to the mouse position from sandboxed scripts.
 */
registerFilterPolicy({
    fakeMouseEventProp: function(propName) {
        const self = this;
        const whichWindow = this.policyApi.whichWindow();

        // Inspired by http://stackoverflow.com/a/33064438
        const descriptor = whichWindow.Object.
            getOwnPropertyDescriptor(
            whichWindow.MouseEvent.prototype, propName);
        this.hookedProps[propName] = descriptor;
        if (descriptor && descriptor.configurable) {
            whichWindow.Object.defineProperty(
                whichWindow.MouseEvent.prototype, propName, {
                get: function() {
                    if (self.policyApi.inSandbox() > 0) {
                        console.log('Blocked mouse tracking on sandbox');
                        return 0;
                    }
                    return descriptor.get.call(this);
                },
                configurable: true,
            });
        } else {
            console.log('Error overriding ' + propName +
                ' (block mouse tracking).');
        }
    },

    unfakeMouseEventProp: function(propName) {
        const whichWindow = this.policyApi.whichWindow();
        const descriptor = this.hookedProps[propName];
        if (descriptor && descriptor.configurable) {
            whichWindow.Object.defineProperty(
                whichWindow.MouseEvent.prototype, propName, {
                get: descriptor.get,
            });
        }
    },

    /**
     * Hook all mouse accesses to filter them if inside a sandboxed script.
     * @param {Object} policyApi Instance of content filter policy API.
     * @param {Object} policyParams Object containing policy parameters.
     */
    onStart: function(policyApi, policyParams) {
        this.policyApi = policyApi;
        this.hookedProps = {};
        this.fakeMouseEventProp('clientX');
        this.fakeMouseEventProp('clientY');
        // You can add more properties here to fake it completely...
    },

	onStop: function() {
        this.unfakeMouseEventProp('clientX');
        this.unfakeMouseEventProp('clientY');
	},
}, '@@INJECT_POLICY_PARAMETERS_HERE@@');
