/*
 * Trace accesses to filtered elements.
 */
registerFilterPolicy({
    onFilteredElementAdded: function(elm) {
        // Log blocked element access
        console.log('ADDED AD ELEMENT:' +
            ' of elm with tag=' + elm.tagName +
            ',id=' + elm.id +
            ',classes=' + elm.className +
            ',display=' + elm.style.getPropertyValue('display') +
            ' ' + elm.style.getPropertyPriority('display'));
    },

    onFilteredElementRemoved: function(elm) {
        // Log blocked element access
        console.log('REMOVED AD ELEMENT:' +
            ' of elm with tag=' + elm.tagName +
            ',id=' + elm.id +
            ',classes=' + elm.className +
            ',display=' + elm.style.getPropertyValue('display') +
            ' ' + elm.style.getPropertyPriority('display'));
    },

    /**
     * Start policy callback.
     * @param {Object} policyApi Instance of content filter policy API.
     * @param {Object} policyParams Object containing policy parameters.
     * - return: The return code to return for the request.
     */
    onStart: function(policyApi, policyParams) {
        this.policyApi = policyApi;
    },
}, '@@INJECT_POLICY_PARAMETERS_HERE@@');
