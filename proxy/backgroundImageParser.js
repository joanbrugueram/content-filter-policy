'use strict';

/**
 * Exchanges single and double quotes characters in a Javascript string.
 * @param  {String} str Input string.
 * @return {String}     Input string with single and double quote characters
 *                      exchanged.
 */
function exchangeSingleAndDoubleQuotes(str) {
    return str.split('"').map(function(p) {
        return p.replace(/'/g, '"');
    }).join('\'');
}

/**
 * Simple parser based on matching input with regular expressions.
 * @param {String} input Input string to parse.
 */
function RegExpBasedParser(input) {
    this.position = 0;
    this.input = input;

    /**
     * Extracts a token from the input and consumes it.
     * @param  {Array} parsers List of parsers to use to use to extract the
     *                         token.
     *                         Each of the parsers is an structure having:
     *                         - A 'type' member which will be returned back.
     *                         - A 'regex' member specifying the regular
     *                           expression to use to parse the token.
     *                           Requires using a starting caret at the start,
     *                           and a non-global match.
     * @return {Array} Matched token structure, or null.
     *                 If a token is matched, result is an structure having:
     *                 - A 'type' member specifying the parser used.
     *                 * A 'content' member containing the matched string.
     */
    this.getToken = function(parsers) {
        if (this.isEnd())
            return null;

        const matchStr = this.input.slice(this.position);

        for (let i = 0; i < parsers.length; i++) {
            const parser = parsers[i];

            const match = parser.regex.exec(matchStr);
            if (match !== null) {
                this.position += match[0].length;

                return {
                    type: parser.type,
                    content: match[0],
                };
            }
        }

        return null;
    };

    /**
     * Checks if we've reached the end of the input.
     * @return {Boolean} true if we are at the end of the input.
     */
    this.isEnd = function() {
        return this.position === this.input.length;
    };

    /**
     * Skips over any whitespace characters in the input.
     */
    this.skipWhiteSpace = function() {
        this.getToken([{
            type: null, // Don't care
            regex: /^\s*/,
        }]);
    };
}

/**
 * Parse a background-image attribute.
 * @param  {String} backgroundImage Value of background-image to parse.
 * @return {Array}                  List of URLs included in background-image.
 */
function parseBackgroundImageUrls(backgroundImage) {
    const urls = [];

    const TOKENTYPE_WHITESPACE = 'WHITESPACE';
    const TOKENTYPE_IDENTIFIER = 'IDENTIFIER';
    const TOKENTYPE_NUMBER = 'NUMBER';
    const TOKENTYPE_OTHER = 'OTHER';
    const TOKENTYPE_SINGLEQUOTEDSTRING = 'SINGLEQUOTEDSTRING';
    const TOKENTYPE_DOUBLEQUOTEDSTRING = 'DOUBLEQUOTEDSTRING';
    const TOKENTYPE_UNQUOTEDSTRING = 'UNQUOTEDSTRING';

    const regularParsers = [{
        type: TOKENTYPE_WHITESPACE,
        regex: /^\s+/,
    }, {
        type: TOKENTYPE_IDENTIFIER,
        regex: /^[_a-zA-Z-]+[_a-zA-Z0-9-]*/,
    }, {
        type: TOKENTYPE_NUMBER,
        regex: /^[+-]?([0-9]*[.])?[0-9]+/,
    }, {
        type: TOKENTYPE_OTHER,
        regex: /^./,
    }];
    const urlParsers = [{
        type: TOKENTYPE_SINGLEQUOTEDSTRING,
        // https://stackoverflow.com/questions/249791
        regex: /^'(?:[^'\\]|\\.)*'/,
    }, {
        type: TOKENTYPE_DOUBLEQUOTEDSTRING,
        // https://stackoverflow.com/questions/249791
        regex: /^"(?:[^"\\]|\\.)*"/,
    }, {
        type: TOKENTYPE_UNQUOTEDSTRING,
        regex: /^[^)]*/,
    }];

    /**
     * Prints an error message relating to an unparseable background-image.
     * @param  {String} message Detailed error message.
     */
    function error(message) {
        throw new Error('Error parsing bg-img ' + backgroundImage +
            ' (' + message + ')');
    }

    const parse = new RegExpBasedParser(backgroundImage);
    let expectKeyword = true;
    let nestLevel = 0;
    while (!parse.isEnd()) {
        if (expectKeyword === true) {
            expectKeyword = false;

            // Get keyword
            parse.skipWhiteSpace();
            const keyword = parse.getToken(regularParsers);
            if (keyword.type !== TOKENTYPE_IDENTIFIER) {
                error('lost track of stream when expecting keyword');
            }

            if (keyword.content === 'url') {
                // Skip starting parenthesis
                parse.skipWhiteSpace();
                const openParenthesis = parse.getToken(regularParsers);
                if (!openParenthesis || openParenthesis.content !== '(') {
                    error('expected open parenthesis after url keyword');
                }

                // Skip ending parenthesis
                parse.skipWhiteSpace();
                const urlString = parse.getToken(urlParsers);
                if (!urlString) {
                    error('expected valid url string');
                }

                // Extract URLs from token content
                if (urlString.type === TOKENTYPE_DOUBLEQUOTEDSTRING) {
                    const unquoted = JSON.parse(urlString.content);
                    urls.push(decodeURI(unquoted.trim()));
                }
                if (urlString.type === TOKENTYPE_SINGLEQUOTEDSTRING) {
                    const unquoted = exchangeSingleAndDoubleQuotes(JSON.parse(
                        exchangeSingleAndDoubleQuotes(urlString.content)));
                    urls.push(decodeURI(unquoted.trim()));
                }
                if (urlString.type === TOKENTYPE_UNQUOTEDSTRING) {
                    urls.push(decodeURI(urlString.content.trim()));
                }

                // Skip ending parenthesis
                parse.skipWhiteSpace();
                const closeParenthesis = parse.getToken(regularParsers);
                if (!closeParenthesis || closeParenthesis.content !== ')') {
                    error('expected close parenthesis after url string');
                }
            }
        } else {
            // Skip over everything, just waiting for root-level commas
            const token = parse.getToken(regularParsers);
            if (token.content === '(') {
                nestLevel++;
            } else if (token.content === ')') {
                if (nestLevel === 0) {
                    error('close parenthesis when nothing open');
                }
                nestLevel--;
            } else if (token.content === ',' && nestLevel === 0) {
                expectKeyword = true;
            }
        }
    }

    // Make sure the state at the end of the parse is consistent
    if (expectKeyword !== false) {
        error('end of stream when expecting keyword');
    }
    if (nestLevel !== 0) {
        error('not all parenthesis closed');
    }

    return urls;
}

/**
 * Run a self-check on parseBackgroundImageUrls (poor man's unit tests).
 * @param  {String} backgroundImage Value of background-image to check.
 * @param  {Array} expected        Expected URL array.
 */
function selfCheckParseBackgroundImageUrls(backgroundImage, expected) {
    const backgroundImageUrls = parseBackgroundImageUrls(backgroundImage);
    if (JSON.stringify(backgroundImageUrls) !== JSON.stringify(expected)) {
        console.log('Self-check FAILED. ' + backgroundImage.toString() + ' --> '
            + backgroundImageUrls + ' instead of ' + expected.toString());
    }
}

selfCheckParseBackgroundImageUrls('none', []);
selfCheckParseBackgroundImageUrls('none, none', []);
selfCheckParseBackgroundImageUrls('none, linear-gradient' +
    '(rgb(255, 248, 186.5) 0%, red 100%)', []);
selfCheckParseBackgroundImageUrls('url(test1.png)', ['test1.png']);
selfCheckParseBackgroundImageUrls('url("test2.png"),url(\'test3.png\')',
                                  ['test2.png', 'test3.png']);
selfCheckParseBackgroundImageUrls('url("test\\"4.png"),url(\'test\\\'5.png\')',
                                  ['test"4.png', 'test\'5.png']);
selfCheckParseBackgroundImageUrls('-webkit-linear-gradient(top, ' +
                                  'rgb(67, 135, 253), rgb(70, 131, 234))', []);

module.exports = {
    parseBackgroundImageUrls: parseBackgroundImageUrls,
};
