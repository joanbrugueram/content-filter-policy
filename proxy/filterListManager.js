'use strict';

const request = require('request');
const fs = require('fs');
const mkdirp = require('mkdirp');
const async = require('async');
const path = require('path');
const ABPFilterParser = require('abp-filter-parser');

const filterListDirectory = 'filterLists';

/**
 * Downloads a file from an URL to a file path.
 * @param  {string}   url             URL from where to download the file.
 * @param  {string}   destinationPath Path where the file should be saved.
 * @param  {Function} cb              Callback which is called after finishing,
 *                                    with an optional error parameter.
 */
function downloadFile(url, destinationPath, cb) {
    const file = fs.createWriteStream(destinationPath);

    const sendReq = request.get(url);
    sendReq.on('response', function(response) {
        if (response.statusCode !== 200) {
            cb(new Error('Response status was ' + response.statusCode));
        }
    });
    sendReq.on('error', function(err) {
        fs.unlink(destinationPath);
        cb(err);
    });
    sendReq.pipe(file);

    file.on('finish', function() {
        file.close(function(err) {
            cb(err);
        });
    });

    file.on('error', function(err) {
        fs.unlink(destinationPath);
        cb(err);
    });
}

/**
 * Downloads a file from an URL to a file path, if it doesn't already exist.
 * @param  {string}   url             URL from where to download the file.
 * @param  {string}   destinationPath Path where the file should be saved.
 * @param  {Function} cb              Callback which is called after finishing,
 *                                    with an optional error parameter.
 */
function downloadFileIfNotExists(url, destinationPath, cb) {
    fs.access(destinationPath, function(err) {
        if (!err) {
            cb();
            return;
        }

        downloadFile(url, destinationPath, cb);
    });
}

/**
 * Gets the path corresponding to a filter list.
 * @param  {Structure} filterList Filter list instance.
 * @return {string}               Filter list file path.
 */
function getFilterListFilePath(filterList) {
    const fileName = filterList.name + '.txt';
    const filePath = path.join(filterListDirectory, fileName);
    return filePath;
}

/**
 * Create a filter list manager with no subscribed filters.
 */
function FilterListManager() {
    this.filterLists = [];
}

/**
 * Add a subscription to a filter list.
 * @param {Structure} subscription Structure of the filter list.
 *                                 Must contain a 'name' and 'url' field.
 */
FilterListManager.prototype.add = function(subscription) {
    if (subscription.name.indexOf('/') !== -1 ||
        subscription.name.indexOf('\\') !== -1)
        throw new Error('Filter list name contains a path separator.');

    this.filterLists.push(subscription);
};

/**
 * Update the subscribed filter lists.
 * @param  {Function} cb              Callback which is called after finishing,
 *                                    with an optional error parameter.
 */
FilterListManager.prototype.update = function(cb) {
    mkdirp(filterListDirectory, (err) => {
        if (err) {
            cb(err);
            return;
        }

        async.map(this.filterLists, function(filterList, asyncCb) {
            const filePath = getFilterListFilePath(filterList);
            downloadFileIfNotExists(filterList.url, filePath, asyncCb);
        }, function(err, results) {
            cb(err);
        });
    });
};

/**
 * Read all subscribed filter lists from their respective files.
 * @param  {Function} cb              Callback which is called after finishing,
 *                                    with an optional error parameter,
 *                                    and an array of filter lists.
 */
FilterListManager.prototype.read = function(cb) {
    const parsedFilterData = {};

    async.eachSeries(this.filterLists, function(filterList, asyncCb) {
        const filePath = getFilterListFilePath(filterList);
        fs.readFile(filePath, function(err, list) {
            if (err) {
                asyncCb(err, null);
                return;
            }

            ABPFilterParser.parse(list.toString(), parsedFilterData);
            asyncCb(null, null);
        });
    }, function(err) {
        if (err) {
            cb(err, null);
            return;
        }

        cb(null, parsedFilterData);
    });
};

module.exports = FilterListManager;
